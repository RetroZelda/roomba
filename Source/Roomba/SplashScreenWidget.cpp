// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "SplashScreenWidget.h"


void USplashScreenWidget::AddScreen(UImage* pImage)
{
	if (pImage != nullptr)
	{
		m_SplashScreens.Add(pImage);
	}
}

void USplashScreenWidget::RemoveScreen(UImage* pImage)
{
	if (pImage != nullptr)
	{
		m_SplashScreens.Remove(pImage);
	}
}

void USplashScreenWidget::NextFadeScreen()
{
	m_fCurFadeTime = 0.0f;
	m_FadeState = FADE_IN;
	m_nSplashScreenIndex++;

	if (m_nSplashScreenIndex >= m_SplashScreens.Num())
	{
		SplashScreenFinished();
		m_FadeState = FADE_NONE;
	}
}

void USplashScreenWidget::SplashScreenFinished_Implementation()
{
	AlphaAllScreens(0.0f);
	RemoveFromParent();
}

void USplashScreenWidget::NativeConstruct()
{
	Super::NativeConstruct();
	m_fCurFadeTime = 0.0f;
	m_FadeState = FADE_DELAY;
	m_nSplashScreenIndex = -1;

}

void USplashScreenWidget::AlphaAllScreens(float fAlpha)
{
	// set all images to be 0
	if (m_SplashScreens.Num() > 0)
	{
		for (int nScreenIndex = 0; nScreenIndex < m_SplashScreens.Num(); ++nScreenIndex)
		{
			SetScreenAlpha(nScreenIndex, fAlpha);
		}
	}
}

void USplashScreenWidget::SetScreenAlpha(int32 nScreenIndex, float fAlpha)
{
	m_SplashScreens[nScreenIndex]->SetOpacity(fAlpha);
}

void USplashScreenWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	m_fCurFadeTime += InDeltaTime;

	switch (m_FadeState)
	{
		case FADE_IN:
		{
			SetScreenAlpha(m_nSplashScreenIndex, m_fCurFadeTime / m_fFadeInTime);
			if (m_fCurFadeTime > m_fFadeInTime)
			{
				m_fCurFadeTime = 0.0f;
				m_FadeState = FADE_OUT;
			}
			break;
		}
		case FADE_HOLD:
		{
			if (m_fCurFadeTime > m_fFadeHoldTime)
			{
				m_fCurFadeTime = 0.0f;
				m_FadeState = FADE_OUT;
			}
			break;
		}
		case FADE_OUT:
		{
			SetScreenAlpha(m_nSplashScreenIndex, 1.0f - (m_fCurFadeTime / m_fFadeOutTime));
			if (m_fCurFadeTime > m_fFadeOutTime)
			{
				m_fCurFadeTime = 0.0f;
				m_FadeState = FADE_DELAY;
			}
			break;
		}
		case FADE_DELAY:
		{
			if (m_fCurFadeTime > m_fFadeDelay)
			{
				NextFadeScreen();
			}
			break;
		}
		case FADE_NONE:
		{
			break;
		}
	}
}

void USplashScreenWidget::SkipSplashScreen()
{
	AlphaAllScreens(0.0f);
	SplashScreenFinished();
}

float USplashScreenWidget::GetTotalTimeOfSplashScreen()
{
	return (2.0f * m_fFadeDelay) + ((m_fFadeInTime + m_fFadeHoldTime + m_fFadeOutTime + m_fFadeDelay)* m_SplashScreens.Num());
}