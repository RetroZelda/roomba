// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "WindZone.h"


// Sets default values
AWindZone::AWindZone(const FObjectInitializer& ObjectInitialize) : Super(ObjectInitialize)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pBounds = ObjectInitialize.CreateDefaultSubobject<UBoxComponent>(this, TEXT("Bounds"));
	RootComponent = m_pBounds;
	m_pBounds->bGenerateOverlapEvents = true;
	m_pBounds->SetNotifyRigidBodyCollision(true);
	m_pBounds->SetMobility(EComponentMobility::Static);
	m_pBounds->SetCollisionProfileName(TEXT("WindZone"));
	m_pBounds->OnComponentBeginOverlap.AddDynamic(this, &AWindZone::OnComponentBeginOverlap);
	m_pBounds->OnComponentEndOverlap.AddDynamic(this, &AWindZone::OnComponentEndOverlap);
}

void AWindZone::OnComponentBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Adding Actor to WindZone!"));
	FOverlappingObjectData* pNewOverlap = new FOverlappingObjectData();
	pNewOverlap->OtherActor = OtherActor;
	pNewOverlap->OtherComp = OtherComp;
	m_OverlappingObjects.Add(pNewOverlap);

}

void AWindZone::OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// remove this actor
	// NOTE: Doing backwards because YOLO
	for (int32 nIndex = m_OverlappingObjects.Num() - 1; nIndex >= 0; --nIndex)
	{
		if (m_OverlappingObjects[nIndex]->OtherActor == OtherActor)
		{
			UE_LOG(LogTemp, Warning, TEXT("Removing Actor from WindZone!"));
			const bool bAllowShrinking = false;
			m_OverlappingObjects.RemoveAt(nIndex, 1, bAllowShrinking);
			return;
		}
	}
}

// Called when the game starts or when spawned
void AWindZone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWindZone::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);
	for (int32 nIndex = 0; nIndex < m_OverlappingObjects.Num(); ++nIndex)
	{
		FOverlappingObjectData* pData = m_OverlappingObjects[nIndex];

		// get a vector from our center to the actor location
		FVector v3Dir = pData->OtherActor->GetActorLocation() - GetActorLocation();
		v3Dir.Normalize();

		// ensure we dont blow it strait up
		if (v3Dir == FVector(0.0f, 0.0f, 1.0f))
		{
			v3Dir += (GetActorForwardVector() / 2.0f);
			v3Dir.Normalize();
		}

		// throw the impulse
		pData->OtherComp->AddImpulse(v3Dir * m_fForce);
	}
}

