// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RoombaGameMode.h"
#include "RoombaEndlessGameMode.generated.h"

class UWaitToStartHUD;
class ARoombaActor;
/**
 * 
 */
UCLASS()
class ROOMBA_API ARoombaEndlessGameMode : public ARoombaGameMode
{
	GENERATED_UCLASS_BODY()

	UClass *m_pStartHudClass;
	UWaitToStartHUD* m_pStartHUD;

	ARoombaActor* m_pRoomba;

protected:

	UFUNCTION()
		virtual void OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState) override;

	UFUNCTION()
		virtual void OnLoadingStart() override;

	UFUNCTION()
		virtual void OnLoadingComplete() override;

	virtual void HandleStateEnter(EGameRunningState eState) override;
	virtual void HandleStateExit(EGameRunningState eState) override;
	virtual void HandleStateTick(EGameRunningState eState, float DeltaSeconds) override;
	
public:
	virtual void LoadInPlayers() override;
	virtual void LoadInHud() override;
};
