// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "ScreenFaderHUD.h"


void UScreenFaderHUD::OnFadeInFinish()
{
	OnScreenFinishedFadeIn.Broadcast();
}

void UScreenFaderHUD::OnFadeOutFinish()
{
	OnScreenFinishedFadeOut.Broadcast();
}

void UScreenFaderHUD::StartFadeIn(float fFadeTime, float fWaitTime)
{
	m_eCurFadeState = EFadeState::FADE_IN_HOLD;
	m_fFadeTime = fFadeTime;
	m_fWaitTime = fWaitTime;
	m_fCurTime = 0.0f;
}

void UScreenFaderHUD::StartFadeOut(float fFadeTime, float fWaitTime)
{
	m_eCurFadeState = EFadeState::FADE_OUT_HOLD;
	m_fFadeTime = fFadeTime;
	m_fWaitTime = fWaitTime;
	m_fCurTime = 0.0f;
}

void UScreenFaderHUD::SetFadeImage(UImage* pImg)
{
	m_pFadeImage = pImg;
}

void UScreenFaderHUD::NativeConstruct()
{
	Super::NativeConstruct();
	m_eCurFadeState = EFadeState::FADE_NONE;
}

void UScreenFaderHUD::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	if (m_pFadeImage != nullptr && m_eCurFadeState != EFadeState::FADE_NONE)
	{
		m_fCurTime += InDeltaTime;
		switch (m_eCurFadeState)
		{
		case EFadeState::FADE_IN_HOLD:
			m_pFadeImage->SetOpacity(0.0f);

			if (m_fCurTime > m_fWaitTime)
			{
				m_eCurFadeState = EFadeState::FADE_IN;
				m_fCurTime = 0.0f;
			}
			break;
		case EFadeState::FADE_IN:
			m_pFadeImage->SetOpacity(m_fCurTime / m_fFadeTime);

			if (m_fCurTime > m_fFadeTime)
			{
				OnFadeInFinish();
				m_eCurFadeState = EFadeState::FADE_NONE;
			}
			break;
		case EFadeState::FADE_OUT_HOLD:
			m_pFadeImage->SetOpacity(1.0f);

			if (m_fCurTime > m_fWaitTime)
			{
				m_eCurFadeState = EFadeState::FADE_OUT;
				m_fCurTime = 0.0f;
			}
			break;
		case EFadeState::FADE_OUT:
			m_pFadeImage->SetOpacity(1.0f - (m_fCurTime / m_fFadeTime));

			if (m_fCurTime > m_fFadeTime)
			{
				OnFadeOutFinish();
				m_eCurFadeState = EFadeState::FADE_NONE;
			}
			break;
		}
	}
}