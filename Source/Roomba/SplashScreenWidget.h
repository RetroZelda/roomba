// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "SplashScreenWidget.generated.h"

/**
 * 
 */
UCLASS()
class ROOMBA_API USplashScreenWidget : public UUserWidget
{
	GENERATED_BODY()

	enum FadeDirection{ FADE_IN, FADE_HOLD, FADE_OUT, FADE_DELAY, FADE_NONE };

	TArray<UImage*> m_SplashScreens;
	int32 m_nSplashScreenIndex;
	FadeDirection m_FadeState;
	float m_fCurFadeTime;

	void NextFadeScreen();
	void SetScreenAlpha(int32 nWidgetIndex, float fAlpha);

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roomba Splash Screen")
	float m_fFadeInTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roomba Splash Screen")
	float m_fFadeHoldTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roomba Splash Screen")
	float m_fFadeOutTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roomba Splash Screen")
	float m_fFadeDelay;

public:

	UFUNCTION(BlueprintCallable, Category = "Roomba Splash Screen")
	void AddScreen(UImage* pImage);

	UFUNCTION(BlueprintCallable, Category = "Roomba Splash Screen")
	void RemoveScreen(UImage* pImage);

	UFUNCTION(BlueprintCallable, Category = "Roomba Splash Screen")
	void AlphaAllScreens(float fAlpha);

	UFUNCTION(BlueprintNativeEvent, Category = "Roomba Splash Screen")
	void SplashScreenFinished();

	UFUNCTION(BlueprintCallable, Category = "Roomba Splash Screen")
	void SkipSplashScreen();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Roomba Splash Screen")
	float GetTotalTimeOfSplashScreen();

	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
};
