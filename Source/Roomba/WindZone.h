// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WindZone.generated.h"

UCLASS()
class ROOMBA_API AWindZone : public AActor
{
	GENERATED_UCLASS_BODY()

	struct FOverlappingObjectData
	{
		class AActor* OtherActor;
		class UPrimitiveComponent* OtherComp;
	};

	TArray<FOverlappingObjectData*> m_OverlappingObjects;

	UPROPERTY(EditANywhere, Category = "WindZone")
	UBoxComponent* m_pBounds;

	UPROPERTY(EditANywhere, Category = "WindZone")
	float m_fForce;

	UFUNCTION()
	void OnComponentBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnComponentEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
