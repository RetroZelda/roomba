// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "WaitToStartHUD.h"


void UWaitToStartHUD::StartAnimationSequence()
{
	PlayAnimation(m_pFadeInAnim);
}

void UWaitToStartHUD::SetAnimation(UWidgetAnimation* pFadeAnim, UWidgetAnimation* pPulseAnim)
{
	m_pFadeInAnim = pFadeAnim;
	m_pPulseAnim = pPulseAnim;
}

void UWaitToStartHUD::NativeConstruct()
{
	Super::NativeConstruct();
}

void UWaitToStartHUD::OnAnimationFinished_Implementation(const UWidgetAnimation* pAnimation)
{
	// Super::OnAnimationFinished(pAnimation); // causes crash?
	if (pAnimation == m_pFadeInAnim)
	{
		PlayAnimation(m_pPulseAnim, 0.0f, 0);
	}
}