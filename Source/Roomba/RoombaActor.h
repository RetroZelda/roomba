// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"

#include "RoombaActor.generated.h"

/** Delegate for notification when the Roomba dies(out of battery) */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRoombaOutOfBattery, class ARoombaActor*, pRoomba);


UCLASS()
class ROOMBA_API ARoombaActor : public APawn
{
	GENERATED_UCLASS_BODY()
		
	UPROPERTY(EditAnywhere, Category = "Roomba")
	UPawnMovementComponent *m_pMovementComponent;

	UPROPERTY(EditAnywhere, Category = "Roomba")
	UStaticMeshComponent *m_pRoombaMesh;

	UPROPERTY(EditAnywhere, Category = "Roomba")
	UAudioComponent* m_pAudioPlayer;
	
public:

	UPROPERTY(BlueprintAssignable, Category = "Roomba")
	FRoombaOutOfBattery OnRoombaOutOfBattery;

private:

	USoundCue* m_pRoombaDeadSound;
	USoundCue* m_pRoombaSound;
	USoundCue* m_pSuckSound;

	float m_fBatteryDrainRate;
	float m_fCurBattery;
	float m_fMaxBattery;
	float m_fTotalTime;

	float m_fCurProgress;
	float m_fMaxProgress;
	float m_fTotalProgress;

	UFUNCTION()
	void OnCollisionOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	UFUNCTION()
	void OnGameStart();

	UFUNCTION()
	void OnGameOver();

	void ChangePitch(float fPitch);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
		
	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetBatteryPercent() { return m_fCurBattery / m_fMaxBattery; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetProgressPercent() { return m_fCurProgress / m_fMaxProgress; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetBatteryDrainRate() { return m_fBatteryDrainRate; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetCurBattery() { return m_fCurBattery; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetMaxBattery() { return m_fMaxBattery; }
	
	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetTotalTime() { return m_fTotalTime; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetCurProgress() { return m_fCurProgress; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetMaxProgress() { return m_fMaxProgress; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetTotalProgress() { return m_fTotalProgress; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	void SetBatteryDrainRate(float fRate) { m_fBatteryDrainRate = fRate; };

	// Drain the battery based on the drain rate
	//		in: delta time
	//		ret: new battery level
	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float DrainBattery(float DeltaTime);

	// Updates our total time
	//		in: delta time
	//		ret: new total time
	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float UpdateAliveTime(float DeltaTime);
};
