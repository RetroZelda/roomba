// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CleanableActor.generated.h"

UCLASS()
class ROOMBA_API ACleanableActor : public AActor
{
	GENERATED_UCLASS_BODY()

		UPROPERTY(EditAnywhere, Category = "Roomba")
		float m_fPointValue;
protected:

public:	

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	float GetPointValue() { return m_fPointValue; }

	UFUNCTION(BlueprintCallable, Category = "Roomba")
	void SetPointValue(float fVal) { m_fPointValue = fVal; }
	

};
