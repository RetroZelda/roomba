// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "RoombaStatics.generated.h"

/**
 * 
 */
UCLASS()
class ROOMBA_API URoombaStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	// TODO:
	// [ ] - Blueprintable string concat for the gamemode init options thing
	// [ ] - blueprintable parsing for string from options into map<str var, str value>

	// NOTE: Final string format: "?var0=val0&var1=val1"

	UFUNCTION(BlueprintCallable, category = "Roomba Statics")
	static FString AppendOptions(FString szCurString, FString szVariable, FString szValue);

	// UFUNCTION(BlueprintCallable, category = "Roomba Statics")
	static FString BuildOptions(TMap<FString, FString>& options);

	// UFUNCTION(BlueprintCallable, category = "Roomba Statics")
	static TMap<FString, FString> ParseOptions(const FString &szOptionsString);
};
