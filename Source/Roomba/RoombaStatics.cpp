// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaStatics.h"

FString URoombaStatics::AppendOptions(FString szCurString, FString szVariable, FString szValue)
{
	if (szCurString != "")
		return szCurString + "&" + szVariable + "=" + szValue;
	else
		return szVariable + "=" + szValue;

}

FString URoombaStatics::BuildOptions(TMap<FString, FString>& options)
{
	FString szFinal = "";
	for (TPair<FString, FString> block : options)
	{
		szFinal = AppendOptions(szFinal, block.Key, block.Value);
	}
	return szFinal;
}

TMap<FString, FString> URoombaStatics::ParseOptions(const FString &szOptionsString)
{
	TMap<FString, FString> finalOptions;
	FString szCurBlock, szKey, szVal;
	FString szCurOptions = szOptionsString;

	// ensure we dont start with a leading '?'
	szCurOptions.RemoveFromStart("?");

	do
	{
		bool bSplit = szCurOptions.Split("&", &szCurBlock, &szCurOptions, ESearchCase::IgnoreCase, ESearchDir::FromStart);
		if (bSplit)
		{
			// split the block into the var and val(key/value) and hold it in the map
			bSplit = szCurBlock.Split("=", &szKey, &szVal, ESearchCase::IgnoreCase, ESearchDir::FromStart);
			if (bSplit)
			{
				finalOptions.Add(szKey, szVal);
			}
		}
		else
		{
			// split the last/only block up
			bSplit = szCurOptions.Split("=", &szKey, &szVal, ESearchCase::IgnoreCase, ESearchDir::FromStart);
			if (bSplit)
			{
				finalOptions.Add(szKey, szVal);
			}
			return finalOptions;
		}
	}
	while (true);
}