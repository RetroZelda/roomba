// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "ScreenFaderHUD.generated.h"



/** Delegate for notification when the screen is finished fading 
	params:
		in: bool bFadeIn - true if finished fading to black; false if finished fading from black
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FScreenFadeFinished);

/**
 * 
 */
UCLASS()
class ROOMBA_API UScreenFaderHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "Screen Fader")
	FScreenFadeFinished OnScreenFinishedFadeIn;

	UPROPERTY(BlueprintAssignable, Category = "Screen Fader")
	FScreenFadeFinished OnScreenFinishedFadeOut;

private:

	enum EFadeState { FADE_IN_HOLD, FADE_IN, FADE_OUT, FADE_OUT_HOLD, FADE_NONE };

	UImage* m_pFadeImage;
	EFadeState m_eCurFadeState;

	float m_fFadeTime;
	float m_fWaitTime;
	float m_fCurTime;
	
protected:

	UFUNCTION(BlueprintCallable, Category = "Screen Fader")
	void OnFadeInFinish();

	UFUNCTION(BlueprintCallable, Category = "Screen Fader")
	void OnFadeOutFinish();

	UFUNCTION(BlueprintCallable, Category = "Screen Fader")
	void SetFadeImage(UImage* pImg);

public:

	UFUNCTION(BlueprintCallable, Category = "Screen Fader")
	void StartFadeIn(float fFadeTime, float fWaitTime);

	UFUNCTION(BlueprintCallable, Category = "Screen Fader")
	void StartFadeOut(float fFadeTime, float fWaitTime);


	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
};
