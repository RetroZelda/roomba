// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "RoombaController.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FScreenTouchEvent, float, fTouchX, float, fTouchY, bool, bTouchState);

/**
 * 
 */
class URoombaHUDBase;
class AArrowActor;
UCLASS()
class ROOMBA_API ARoombaController : public APlayerController
{
	// TODO: Make this a base controller, move logic from here into a RoombaTouchController, and link the TwinstickController up to the base roomba controller
	GENERATED_UCLASS_BODY()
		
public:
	enum EMovementState { IDLE, TURN, MOVE, DEAD };

	UPROPERTY(BlueprintAssignable, Category = "Roomba Controller")
	FScreenTouchEvent OnScreenTouch;
	
private:

	AArrowActor* m_pArrow;

	EMovementState m_eState;
	FVector m_v3MoveDir;
	UClass* m_pArrowBP;
	float m_fAngle;
	float m_fTurnRate;

	UClass *m_pHUDClass;
	UClass *m_pGameOverHUDClass;
	URoombaHUDBase *m_pHUD;

	bool m_bTouchState;

	void StartMovement();
	void StopMovement();

	UFUNCTION()
	void OnRoombaOutOfBattery(ARoombaActor* pRoomba);

protected:

	UFUNCTION(Category = "Roomba Controller")
	virtual void AddToCamera(APawn* pAttachPawn);

	UFUNCTION(Category = "Roomba Controller")
	virtual void RemoveFromCamera(APawn* pAttachPawn);

public:

	virtual void Possess(APawn* aPawn) override;
	virtual void UnPossess() override;
	virtual void BeginPlay() override;

	void SetupInputComponent();
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE EMovementState GetMovementState() { return m_eState; }
	
};
