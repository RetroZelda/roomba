// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "Maze.h"
#include "MazeObject.h"
#include "MazeTile.h"
#include "CleanableActor.h"
#include "RoombaPlayerStart.h"

// Sets default values
AMaze::AMaze()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_nPelletCount = 0;
	_nPowerPelletCount = 0;
	_fGridSize = 10.0f;
}

// Called when the game starts or when spawned
void AMaze::BeginPlay()
{
	Super::BeginPlay();
	BuildMaze(_pCurrentMazeLayout);
}

// Called every frame
void AMaze::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


void AMaze::BuildMaze(UMazeObject* pMaze)
{
	uint32 nWidth = pMaze->GetWidth();
	uint32 nHeight = pMaze->GetHeight();
	uint32 nCount = pMaze->GetTileCount();
	_nPelletCount = _nPowerPelletCount = 0;

	FVector v3MyScale = GetActorScale();
	FVector v3TilePosOffset = FVector(nWidth * _fGridSize * v3MyScale.X, nHeight * _fGridSize * v3MyScale.Y, 0.0f) / 2.0f;

	AActor* pLayoutActor = nullptr;
	AActor* pPelletActor = nullptr;
	FRotator rTileLocalRot;
	FVector v3TileLocalScale = FVector(1.0f, 1.0f, 1.0f);

	FActorSpawnParameters spawnParams;
	
	for (uint32 W = 0; W < nWidth; ++W)
	{
		for (uint32 H = 0; H < nHeight; ++H)
		{
			EMazeTileType eTile = pMaze->GetTile(W * nHeight + H);
			FVector v3TileLocalPos = FVector(W * _fGridSize * v3MyScale.X, H * _fGridSize * v3MyScale.Y, 0.0f) - v3TilePosOffset;

			switch (eTile)
			{
			case EMazeTileType::Wall_Ghost_Door:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 90.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pDoorTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_Horizontal:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 90.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pWallTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_Verticle:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 0.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pWallTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_N_E:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 270.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pCornerTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_N_W:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 180.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pCornerTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_S_E:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 0.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pCornerTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Wall_S_W:
				rTileLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 90.0f)));
				pLayoutActor = GetWorld()->SpawnActor(*_pCornerTile, &v3TileLocalPos, &rTileLocalRot);
				break;
			case EMazeTileType::Invalid:
				break;
			case EMazeTileType::Floor_Ghost_Only:
			case EMazeTileType::Floor_Empty:
			Floor :
				pLayoutActor = GetWorld()->SpawnActor(*_pFloorTile, &v3TileLocalPos);
				break;
			case EMazeTileType::Floor_Pellet:
				pPelletActor = GetWorld()->SpawnActor(*_pPellet, &v3TileLocalPos);
				_nPelletCount++;
				goto Floor;
				break;
			case EMazeTileType::Floor_Power_Pellet:
				pPelletActor = GetWorld()->SpawnActor(*_pPowerPellet, &v3TileLocalPos);
				_nPowerPelletCount++;
				goto Floor;
				break;
			}

			if (pLayoutActor != nullptr)
			{
				pLayoutActor->AttachRootComponentToActor(this, NAME_None, EAttachLocation::KeepRelativeOffset, true);
				pLayoutActor->SetActorRelativeLocation(v3TileLocalPos);
				pLayoutActor->SetActorRelativeRotation(rTileLocalRot);
				pLayoutActor = nullptr;
			}

			if (pPelletActor != nullptr)
			{
				pPelletActor->AttachRootComponentToActor(this, NAME_None, EAttachLocation::KeepRelativeOffset, false);
				pPelletActor->SetActorRelativeLocation(v3TileLocalPos + FVector(0.0f, 0.0f, _fGridSize));
				pPelletActor->SetActorRelativeRotation(rTileLocalRot);
				pPelletActor = nullptr;
			}
		}
	}

	// spawn the player starts
	for (int32 nIndex = 0; nIndex < 5; ++nIndex)
	{
		uint32 nStartIndex = pMaze->GetPlayerStart(nIndex);
		uint32 W = nStartIndex / nHeight;
		uint32 H = nStartIndex % nHeight;
		FVector v3LocalPos = FVector(W * _fGridSize * v3MyScale.X, H * _fGridSize * v3MyScale.Y, _fGridSize) - v3TilePosOffset;
		FRotator rLocalRot = FRotator(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, 0.0f)));
		AActor* pStart = GetWorld()->SpawnActor(*_pPlayerStart, &v3LocalPos);
		pStart->AttachRootComponentToActor(this, NAME_None, EAttachLocation::KeepRelativeOffset, false);
		pStart->SetActorRelativeLocation(v3LocalPos);
		pStart->SetActorRelativeRotation(rLocalRot);
	}

}