// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "UnrealEd.h"
#include "MazeObjectFactory.h"
#include "MazeObject.h"


UMazeObjectFactory::UMazeObjectFactory()
{
	SupportedClass = UMazeObject::StaticClass();
	Formats.Add("maz;A Maze file");
	bEditorImport = true;
	bCreateNew = false;
}

UObject* UMazeObjectFactory::FactoryCreateBinary
(
	UClass*				Class,
	UObject*			InParent,
	FName				Name,
	EObjectFlags		Flags,
	UObject*			Context,
	const TCHAR*		FileType,
	const uint8*&		Buffer,
	const uint8*		BufferEnd,
	FFeedbackContext*	Warn
)
{
	FEditorDelegates::OnAssetPreImport.Broadcast(this, Class, InParent, Name, FileType);

	UMazeObject* const pMaze = NewObject<UMazeObject>(InParent, Class, Name, Flags);
	if (pMaze)
	{
		pMaze->Initialize(Buffer, BufferEnd - Buffer);
	}

	FEditorDelegates::OnAssetPostImport.Broadcast(this, pMaze);
	return pMaze;
}