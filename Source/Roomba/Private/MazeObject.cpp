// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "MazeObject.h"

UMazeObject::UMazeObject()
{
	memset(&_nPlayerStartIndex, 0, sizeof(uint32) * 5);
	_nMazeWidth = _nMazeHeight = 0;
}

UMazeObject::~UMazeObject()
{
}

void UMazeObject::SplitTile(uint8& tileInfoIn, EMazeTileType& leftOut, EMazeTileType& rightOut)
{
	leftOut = (EMazeTileType)(tileInfoIn >> 4);
	rightOut = (EMazeTileType)(tileInfoIn & 0x0F);
}

bool UMazeObject::Initialize(const void* const InMazeData, const int32 InMazeDataSizeBytes)
{
	// our variables
	int32 nMapSize;
	rsize_t uintSize = sizeof(uint32);
	uint32 nDataOffset = 0;
	uint8 nTileData;
	EMazeTileType left, right;
	
	// load in the map data
	memcpy_s(&_nMazeWidth, uintSize, InMazeData, uintSize);
	nDataOffset += uintSize;
	memcpy_s(&_nMazeHeight, uintSize, (const uint8* const)InMazeData + nDataOffset, uintSize);
	nDataOffset += uintSize;
	memcpy_s(&_nPlayerStartIndex, uintSize * 5, (const uint8* const)InMazeData + nDataOffset, uintSize * 5);
	nDataOffset += (uintSize * 5);

	// load and unsplit each tile
	nMapSize = _nMazeWidth * _nMazeHeight;
	_TileMap.Reserve(nMapSize);
	_pRaw.Reserve(InMazeDataSizeBytes);
	for (; nDataOffset < (uint32)InMazeDataSizeBytes; ++nDataOffset)
	{
		memcpy_s(&nTileData, 1, (const uint8* const)InMazeData + nDataOffset, 1);
		SplitTile(nTileData, left, right);
		_pRaw.Add(nTileData);

		if (_TileMap.Num() < nMapSize)
			_TileMap.Add(left);
		if (_TileMap.Num() < nMapSize)
			_TileMap.Add(right);
	}

	return true;
}

