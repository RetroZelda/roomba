// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "LevelDataAsset.generated.h"


UENUM()
enum class ELevelDataType : uint8
{
	SINGLE 	UMETA(DisplayName = "Single Player Levels"),
	LOCAL 	UMETA(DisplayName = "Local Multiplayer Levels")
};


USTRUCT()
struct FLevelInfo 
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FString DisplayName;

	UPROPERTY(EditAnywhere)
	FString LevelName;

	UPROPERTY(EditAnywhere)
	bool ExcludeMobile;
};

/**
 * 
 */
UCLASS()
class ULevelDataAsset : public UDataAsset
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		ELevelDataType LevelPackType;

	UPROPERTY(EditAnywhere)
	TArray<FLevelInfo> LevelList;
	
	
};
