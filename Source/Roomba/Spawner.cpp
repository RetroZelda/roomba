// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "Spawner.h"


// Sets default values
ASpawner::ASpawner(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pSpawnBounds = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, "SpawnBounds");
	RootComponent = m_pSpawnBounds;

	m_pSpawnBounds->SetCollisionProfileName(TEXT("NoCollision"));
	m_pSpawnBounds->SetBoxExtent(FVector(1.0f, 1.0f, 1.0f), false);

	ConstructorHelpers::FObjectFinder<UClass> paperBP(TEXT("Blueprint'/Game/Paper_Blueprint.Paper_Blueprint_C'")); // TODO: Load in the bp
	m_pActorTypeToSpawn = paperBP.Object;

	m_fSpawnRate = 5.0f;
	m_fBurstChance = 0.7f;
	m_nBurstSizeMin = 3;
	m_nBurstSizeMax = 5;
	m_bRandomRotation = false;
	m_bSpawnOnStart = false;
	m_bIsSpawning = false;
}

FVector ASpawner::GetRandomSpawnPoint()
{
	FVector v3Extents = m_pSpawnBounds->GetScaledBoxExtent();
	FVector v3MyPos = this->GetActorLocation();
	float fX = FMath::FRandRange(-v3Extents.X, v3Extents.X);
	float fY = FMath::FRandRange(-v3Extents.Y, v3Extents.Y);
	float fZ = FMath::FRandRange(-v3Extents.Z, v3Extents.Z);
	return v3MyPos + FVector(fX, fY, fZ);
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	if (m_bSpawnOnStart)
	{
		StartSpawner();
	}
}

void ASpawner::EndPlay(const EEndPlayReason::Type endPlayReason)
{
	Super::EndPlay(endPlayReason);
	StopSpawner();
}

// Called every frame
void ASpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ASpawner::TimeToSpawnSomething()
{
	// check for burst
	float fRoll = FMath::FRand();
	if (fRoll < m_fBurstChance)
	{
		SpawnBurst(FMath::RandRange(m_nBurstSizeMin, m_nBurstSizeMax));
	}
	else
	{
		SpawnSingle();
	}
}

void ASpawner::SpawnSingle()
{
	SpawnBurst(1);
}

void ASpawner::SpawnBurst(uint32 nCount)
{
	UWorld* pWorld = GetWorld();
	for (uint32 nSpawnCount = 0; nSpawnCount < nCount; ++nSpawnCount)
	{
		FVector spawnPos = GetRandomSpawnPoint();
		FVector spawnRot;
		if (m_bRandomRotation)
		{
			spawnRot = GetRandomSpawnPoint();
			spawnRot.Normalize();
		}
		else
		{
			spawnRot = FVector(FVector::UpVector);
		}

		FRotator finalSpawnRot(FRotator::MakeFromEuler(spawnRot));
		AActor* pSpawnedActor = pWorld->SpawnActor(m_pActorTypeToSpawn, &spawnPos, &finalSpawnRot);
	}
}

void ASpawner::StartSpawner()
{
	if (!m_bIsSpawning)
	{
		m_bIsSpawning = true;
		GetWorldTimerManager().SetTimer(m_TimerHandle, this, &ASpawner::TimeToSpawnSomething, m_fSpawnRate, true, 5.0f);
	}
}

void ASpawner::StopSpawner()
{
	if (m_bIsSpawning)
	{
		m_bIsSpawning = false;
		GetWorldTimerManager().ClearTimer(m_TimerHandle);
	}
}