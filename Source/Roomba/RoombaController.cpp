// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaController.h"
#include "RoombaActor.h"
#include "RoombaHUDBase.h"
#include "ArrowActor.h"
#include "RoombaCamera.h"

ARoombaController::ARoombaController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bAutoManageActiveCameraTarget = false;
	bShowMouseCursor = true;
	m_bTouchState = false;
	
	// get the roomba HUD
	static ConstructorHelpers::FObjectFinder<UClass> hudBP(TEXT("WidgetBlueprint'/Game/HUD/RoombaHUD.RoombaHUD_C'"));
	m_pHUDClass = hudBP.Object;

	// get the game over hud
	static ConstructorHelpers::FObjectFinder<UClass> hudGameOver(TEXT("WidgetBlueprint'/Game/HUD/GameOverHUD.GameOverHUD_C'"));
	m_pGameOverHUDClass = hudGameOver.Object;

	// get hte arrow BP
	static ConstructorHelpers::FObjectFinder<UClass> arrowBP(TEXT("Blueprint'/Game/Arrow_Blueprint.Arrow_Blueprint_C'"));
	m_pArrowBP = (UClass*)arrowBP.Object;
	m_pArrow = nullptr;
	
	m_fTurnRate = 270.0f;
	m_eState = EMovementState::IDLE;
}

void ARoombaController::SetupInputComponent()
{
	UE_LOG(LogTemp, Warning, TEXT("ARoombaController::SetupInputComponent()"));

	// set up gameplay key bindings
	Super::SetupInputComponent();

	// bind our actions
	InputComponent->BindAction("Movement", EInputEvent::IE_Pressed, this, &ARoombaController::StartMovement);
	InputComponent->BindAction("Movement", EInputEvent::IE_Released, this, &ARoombaController::StopMovement);
	m_bTouchState = false;
}

void ARoombaController::AddToCamera(APawn* pAttachPawn)
{
	// NOTE: If there are multiple roomba cameras, the last one in this iterator will be what we are set to.
	for (TActorIterator<ARoombaCamera> iter(GetWorld()); iter; ++iter)
	{
		iter->FollowActor(pAttachPawn);
		this->SetViewTarget(*iter);
	}
}

void ARoombaController::RemoveFromCamera(APawn* pAttachPawn)
{
	for (TActorIterator<ARoombaCamera> iter(GetWorld()); iter; ++iter)
	{
		iter->UnfollowActor(pAttachPawn);
	}
	this->SetViewTarget(nullptr);
}

void ARoombaController::Possess(APawn* aPawn)
{
	// set the arrow on the controlling pawn
	UWorld* const pWorld = GetWorld();
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = aPawn;
	m_pArrow = pWorld->SpawnActor<AArrowActor>(m_pArrowBP, SpawnParams);
	m_pArrow->SetActorLocation(aPawn->GetActorLocation());

	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(aPawn);
	pPawn->OnRoombaOutOfBattery.AddDynamic(this, &ARoombaController::OnRoombaOutOfBattery);

	m_pHUD->AddToViewport();
	m_eState = EMovementState::IDLE;

	AddToCamera(aPawn);
	
	Super::Possess(aPawn);
}

void ARoombaController::UnPossess()
{
	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn)
	{
		pPawn->OnRoombaOutOfBattery.RemoveDynamic(this, &ARoombaController::OnRoombaOutOfBattery);
		RemoveFromCamera(pPawn);
	}

	if (m_pArrow)
	{
		m_pArrow->Destroy();
		m_pArrow = nullptr;
	}

	m_pHUD->RemoveFromViewport();
	m_eState = EMovementState::DEAD;

	Super::UnPossess();
}

void ARoombaController::BeginPlay()
{
	Super::BeginPlay();
	m_pHUD = CreateWidget<URoombaHUDBase>(this, m_pHUDClass);
	
	m_v3MoveDir = FVector::ZeroVector;
	m_eState = EMovementState::DEAD;
}

void ARoombaController::Tick(float DeltaTime)
{
	float fX, fY;
	bool bCurTouchState = false;
	GetInputTouchState(ETouchIndex::Touch1, fX, fY, bCurTouchState);

	if (bCurTouchState != m_bTouchState)
	{
		if (bCurTouchState)
		{
			StartMovement();
		}
		else
		{
			StopMovement();
		}
		m_bTouchState = bCurTouchState;
		OnScreenTouch.Broadcast(fX, fY, bCurTouchState);
	}

	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn)
	{
		switch (m_eState)
		{
			case EMovementState::IDLE:
			{
				m_fAngle += m_fTurnRate * DeltaTime;
				while (m_fAngle > 360.0f)
					m_fAngle -= 360.0f;
				while (m_fAngle < 0.0f)
					m_fAngle += 360.0f;

				m_pArrow->SetActorLocation(pPawn->GetActorLocation() + FVector(0.0f, 0.0f, 25.0f));
				m_pArrow->SetActorRelativeRotation(FRotator::MakeFromEuler(FVector(0.0f, 0.0f, m_fAngle)));
				break;
			}
			case EMovementState::TURN:
			{
				FRotator rMyRot = pPawn->GetActorRotation();
				FRotator rDestRot(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, m_fAngle)));
				FRotator rFinalRot = FMath::RInterpConstantTo(rMyRot, rDestRot, DeltaTime, m_fTurnRate);

				pPawn->SetActorRotation(rFinalRot);
				if (rFinalRot.Equals(rDestRot))
				{
					pPawn->ChangePitch(1.0f);
					m_eState = EMovementState::MOVE;
				}

				break;
			}
			case EMovementState::MOVE:
			{
				pPawn->AddMovementInput(pPawn->GetActorForwardVector(), 0.75f);
				break;
			}
		};
	}
}

void ARoombaController::OnRoombaOutOfBattery(ARoombaActor* pRoomba)
{
	m_pArrow->SetActorHiddenInGame(true);
	m_eState = EMovementState::DEAD;

	// show the game over hud
	URoombaHUDBase* pHUD = CreateWidget<URoombaHUDBase>(this, m_pGameOverHUDClass);
	pHUD->AddToViewport();
}

void ARoombaController::StartMovement()
{
	if (m_eState == EMovementState::DEAD)
		return;

	UE_LOG(LogTemp, Warning, TEXT("ARoombaController::StartMovement()"));


	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn)
	{
		pPawn->ChangePitch(2.0f);
		m_eState = EMovementState::TURN;

		// determin our movement dir
		float fRad = FMath::DegreesToRadians(m_fAngle);
		m_v3MoveDir = FVector(FMath::Cos(fRad), FMath::Sin(fRad), 0.0f);

		// hide the arrow
		m_pArrow->SetActorHiddenInGame(true);
	}
}

void ARoombaController::StopMovement()
{
	UE_LOG(LogTemp, Warning, TEXT("ARoombaController::StopMovement()"));
	if (m_eState == EMovementState::DEAD)
		return;

	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn)
	{
		pPawn->ChangePitch(0.5f);
		m_eState = EMovementState::IDLE;
		m_pArrow->SetActorHiddenInGame(false);
	}
}



