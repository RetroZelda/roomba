// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class ROOMBA_API ASpawner : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = "Spawner")
	UBoxComponent* m_pSpawnBounds;

	UPROPERTY(EditAnywhere, Category = "Spawner", meta = (ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "100.0"))
	float m_fSpawnRate;

	UPROPERTY(EditAnywhere, Category = "Spawner", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float m_fBurstChance; 

	UPROPERTY(EditAnywhere, Category = "Spawner", meta = (ClampMin = "0", ClampMax = "100", UIMin = "0", UIMax = "100"))
	int32 m_nBurstSizeMin;

	UPROPERTY(EditAnywhere, Category = "Spawner", meta = (ClampMin = "0", ClampMax = "100", UIMin = "0", UIMax = "100"))
	int32 m_nBurstSizeMax;

	UPROPERTY(EditAnywhere, Category = "Spawner")
	bool m_bRandomRotation;

	// TArray<UClass*> m_SpawnableActors;
	UPROPERTY(EditAnywhere, Category = "Spawner")
	UClass* m_pActorTypeToSpawn;

	UPROPERTY(EditAnywhere, Category = "Spawner")
	bool m_bSpawnOnStart;

private:

	bool m_bIsSpawning;
	FTimerHandle m_TimerHandle;

protected:
	virtual FVector GetRandomSpawnPoint();
	virtual void TimeToSpawnSomething();

public:	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type endPlayReason) override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void SpawnSingle();
	virtual void SpawnBurst(uint32 nCount);

	UFUNCTION(BlueprintCallable, Category = "Spawner")
	virtual void StartSpawner();

	UFUNCTION(BlueprintCallable, Category = "Spawner")
	virtual void StopSpawner();
	
};
