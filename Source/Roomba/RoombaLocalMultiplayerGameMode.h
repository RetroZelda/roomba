// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RoombaGameMode.h"
#include "RoombaLocalMultiplayerGameMode.generated.h"


class APlayerController;
class ARoombaActor;
class UWaitToStartHUD;

/**
 * 
 */
UCLASS()
class ROOMBA_API ARoombaLocalMultiplayerGameMode : public ARoombaGameMode
{
	GENERATED_UCLASS_BODY()


	// NOTE: Starting this class setup like ARoombaEndlessGameMode for the multiplayeral transition
	//		 [X] - This class will need a global camera that adjusts to player positions(Super Smash Bros)
	//		 [ ] - Its Start hud will be that of everyone pressing a button to start and/or a countdown
	//		 [X] - We need to set and handle the player controller spawning and possessing(and setting the class in hte constructor)
	//		 [X] - we need to setup the logic to load a level layout to get the loading screen working(will be in ARoombaGameMode)
	//		 [X] - ensure ARoombaEndlessGameMode uses this logic as well
	UClass *m_pStartHudClass;
	UWaitToStartHUD* m_pStartHUD;


private:

	struct PawnControllerLink
	{
		APlayerController* pController;
		ARoombaActor* pRoomba;

		PawnControllerLink(APlayerController* c, ARoombaActor* a)
		{
			pController = c;
			pRoomba = a;
		}
	};

	TArray<PawnControllerLink> m_Players;
	
protected:

	UFUNCTION()
		virtual void OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState) override;

	UFUNCTION()
		virtual void OnLoadingStart() override;

	UFUNCTION()
		virtual void OnLoadingComplete() override;

	virtual void HandleStateEnter(EGameRunningState eState) override;
	virtual void HandleStateExit(EGameRunningState eState) override;
	virtual void HandleStateTick(EGameRunningState eState, float DeltaSeconds) override;

public:
	virtual void LoadInPlayers() override;
	virtual void LoadInHud() override;
	
	
};
