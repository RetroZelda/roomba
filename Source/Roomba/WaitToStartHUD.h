// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "WaitToStartHUD.generated.h"

/**
 * 
 */
UCLASS()
class ROOMBA_API UWaitToStartHUD : public UUserWidget
{
	GENERATED_BODY()
		
	UWidgetAnimation* m_pFadeInAnim;
	UWidgetAnimation* m_pPulseAnim;

public:

	UFUNCTION(BlueprintCallable, Category = "Wait To Start HUD")
	void StartAnimationSequence();	

	UFUNCTION(BlueprintCallable, Category = "Wait To Start HUD")
	void SetAnimation(UWidgetAnimation* pFadeAnim, UWidgetAnimation* pPulseAnim);

	virtual void NativeConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* pAnimation) override;
};
