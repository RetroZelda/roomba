// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaGameMode.h"
#include "ScreenFaderHUD.h"
#include "LoadingScreenWidget.h"
#include "RoombaCamera.h"


ARoombaGameMode::ARoombaGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = NULL;
	PlayerControllerClass = APlayerController::StaticClass();

	// grab our hud classes
	static ConstructorHelpers::FObjectFinder<UClass> hudFader(TEXT("WidgetBlueprint'/Game/HUD/FadeHUD.FadeHUD_C'"));
	static ConstructorHelpers::FObjectFinder<UClass> hudLoading(TEXT("WidgetBlueprint'/Game/HUD/LoadingHUD.LoadingHUD_C'"));
	m_pFaderHudClass = hudFader.Object;
	m_pLoadingHudClass = hudLoading.Object;

	// default game state
	m_eGameState = EGameRunningState::NONE;
}

void ARoombaGameMode::OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState)
{

}

void ARoombaGameMode::OnScreenFinishedFadeIn()
{

}

void ARoombaGameMode::OnScreenFinishedFadeOut()
{
	SetNewState(EGameRunningState::WAITING_TO_START);
}

void ARoombaGameMode::HandleStreamCompleted()
{
	// TODO: Handle loading in other streamed crap here if needed
	OnLoadingComplete();
}

void ARoombaGameMode::OnLoadingStart()
{

}

void ARoombaGameMode::OnLoadingComplete()
{
	SpawnCamera();
	LoadInPlayers();
	SetNewState(EGameRunningState::FADE_IN);
}


void ARoombaGameMode::HandleStateEnter(EGameRunningState eState)
{
	switch (eState)
	{
		case EGameRunningState::LOADING:
		{
			m_pLoadingHUD->AddToViewport();

			// set to start loading in the level
			FLatentActionInfo streamInfo;
			streamInfo.CallbackTarget = this;
			streamInfo.ExecutionFunction = "HandleStreamCompleted";
			streamInfo.UUID = 1;
			streamInfo.Linkage = 0;
			
			// start the actual streaming
			// NOTE: Might have to handle an unload at some point when we restart or something
			// TODO: Hold the level string somewhere else
			UGameplayStatics::LoadStreamLevel(this, m_szLevelToLoad, true, false, streamInfo);
			OnLoadingStart();
			break;
		}
		case EGameRunningState::FADE_IN:
		{
			m_pFaderHUD->AddToViewport();
			m_pFaderHUD->StartFadeOut(1.0f, 0.25f); // TODO: Make these properties

			break;
		}
		case EGameRunningState::WAITING_TO_START:
		{
			break;
		}
		case EGameRunningState::RUNNING:
		{
			OnGameStart.Broadcast();
			break;
		}
		case EGameRunningState::GAMEOVER:
		{
			OnGameOver.Broadcast();
			break;
		}
		case EGameRunningState::FADE_OUT:
		{
			m_pFaderHUD->AddToViewport();
			m_pFaderHUD->StartFadeIn(1.0f, 0.25f); // TODO: Make these properties
			break;
		}
	}
}

void ARoombaGameMode::HandleStateExit(EGameRunningState eState)
{
	switch (eState)
	{
		case EGameRunningState::LOADING:
		{
			m_pLoadingHUD->RemoveFromViewport();
			break;
		}
		case EGameRunningState::FADE_IN:
		{
			m_pFaderHUD->RemoveFromViewport();
			break;
		}
		case EGameRunningState::WAITING_TO_START:
		{
			break;
		}
		case EGameRunningState::RUNNING:
		{
			break;
		}
		case EGameRunningState::GAMEOVER:
		{
			break;
		}
		case EGameRunningState::FADE_OUT:
		{
			break;
		}
	}
}

void ARoombaGameMode::HandleStateTick(EGameRunningState eState, float DeltaSeconds)
{
	switch (eState)
	{
	case EGameRunningState::LOADING:
	{
		break;
	}
	case EGameRunningState::FADE_IN:
	{
		break;
	}
	case EGameRunningState::WAITING_TO_START:
	{
		break;
	}
	case EGameRunningState::RUNNING:
	{
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		break;
	}
	}
}

void ARoombaGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	// start the load in procedure
	SetNewState(EGameRunningState::LOADING);
}

// NOTE: Options is setup like a url (e.g. "?Option")
void ARoombaGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	// TODO: Get the options in a map<string, string> and parse from a "?var0=val0&var1=val1" like string
	UE_LOG(LogTemp, Warning, TEXT("OPTIONS: %S"), *Options);
	m_GameOptions = URoombaStatics::ParseOptions(Options);

	AssertGameOptions(m_GameOptions);


	LoadInHud();
}

void ARoombaGameMode::AssertGameOptions(TMap<FString, FString>& options)
{
	checkf(options.Contains("level"), TEXT("Missing the \"level\" option for this GameMode."));
	checkf(options.Contains("map"), TEXT("Missing the \"map\" option for this GameMode."));
	m_szLevelToLoad = FName(*(options["map"]));
}

void ARoombaGameMode::SpawnCamera()
{
	// we will generally always only need 1 camera, unless looking for split-screen or something
	m_pCamera = GetWorld()->SpawnActor<ARoombaCamera>();
}

void ARoombaGameMode::LoadInPlayers()
{

}

void ARoombaGameMode::LoadInHud()
{
	// create the hud objects
	m_pFaderHUD = CreateWidget<UScreenFaderHUD>(GetWorld(), m_pFaderHudClass);
	m_pLoadingHUD = CreateWidget<ULoadingScreenWidget>(GetWorld(), m_pLoadingHudClass);

	// bind to the fader's events
	m_pFaderHUD->OnScreenFinishedFadeIn.AddDynamic(this, &ARoombaGameMode::OnScreenFinishedFadeIn);
	m_pFaderHUD->OnScreenFinishedFadeOut.AddDynamic(this, &ARoombaGameMode::OnScreenFinishedFadeOut);
}


void ARoombaGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	HandleStateTick(m_eGameState, DeltaSeconds);
}


void ARoombaGameMode::SetNewState(EGameRunningState eNewState)
{
	// exit the old; enter the new
	HandleStateExit(m_eGameState);
	m_eGameState = eNewState;
	HandleStateEnter(m_eGameState);
}
