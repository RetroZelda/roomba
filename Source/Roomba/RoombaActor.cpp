// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaActor.h"
#include "RoombaHUDBase.h"
#include "CleanableActor.h"
#include "RoombaController.h"
#include "RoombaGameMode.h"

#define ADDCOMPONENT(comp, compClass, owner, text)\
	comp = ObjectInitialize.CreateDefaultSubobject < compClass >(owner, TEXT(text));\
	comp->AttachParent = owner;
// if in actor: comp->RegisterComponent();  if in SceneComponent: comp->AttachParent = owner

#define LOADASSET(varName, assetType, path) static ConstructorHelpers::FObjectFinder<assetType> varName(TEXT(path));


// Sets default values
ARoombaActor::ARoombaActor(const FObjectInitializer& ObjectInitialize)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bFindCameraComponentWhenViewTarget = false;

	m_pRoombaMesh = ObjectInitialize.CreateDefaultSubobject<UStaticMeshComponent>(this, "RoombaMesh");
	RootComponent = m_pRoombaMesh;

	// to allow us to move
	m_pMovementComponent = ObjectInitialize.CreateDefaultSubobject<UPawnMovementComponent, UFloatingPawnMovement>(this, ADefaultPawn::MovementComponentName);
	m_pMovementComponent->UpdatedComponent = RootComponent;

	// set the roomba mesh and material
	static ConstructorHelpers::FObjectFinder<UMaterial> roombaMat(TEXT("Material'/Game/Mesh/Roomba/RoombaBody.RoombaBody'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> cylinderMesh(TEXT("StaticMesh'/Game/Mesh/Roomba/RobotRoomba620.RobotRoomba620'"));
	m_pRoombaMesh->SetSimulatePhysics(true);
	m_pRoombaMesh->SetNotifyRigidBodyCollision(true);
	m_pRoombaMesh->bGenerateOverlapEvents = true;
	m_pRoombaMesh->SetCollisionProfileName("Roomba");
	m_pRoombaMesh->StaticMesh = cylinderMesh.Object;
	// m_pRoombaMesh->SetMaterial(0, roombaMat.Object);
	m_pRoombaMesh->SetRelativeLocation(FVector::ZeroVector);
	m_pRoombaMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
			

	// get our sounds
	m_pAudioPlayer = ObjectInitialize.CreateDefaultSubobject<UAudioComponent>(this, TEXT("AudioPlayer"));
	m_pAudioPlayer->AttachParent = RootComponent;
	static ConstructorHelpers::FObjectFinder<USoundCue> noPowerSound(TEXT("SoundCue'/Game/Sounds/BatteryDrained.BatteryDrained'"));
	static ConstructorHelpers::FObjectFinder<USoundCue> runningSound(TEXT("SoundCue'/Game/Sounds/RoombaSound.RoombaSound'"));
	static ConstructorHelpers::FObjectFinder<USoundCue> suckySound(TEXT("SoundCue'/Game/Sounds/SuckItem.SuckItem'"));
	m_pRoombaDeadSound = noPowerSound.Object;
	m_pRoombaSound = runningSound.Object;
	m_pSuckSound = suckySound.Object;
	

	m_fBatteryDrainRate = 1.0f;
	m_fMaxBattery = 40.0f;
	m_fMaxProgress = 10;
}


void ARoombaActor::OnCollisionOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// handle when we his a cleanable object
	if ((OtherActor != NULL) && OtherActor->IsA(ACleanableActor::StaticClass()))
	{
		OtherActor->Destroy();

		// play the sucky sound
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), m_pSuckSound, GetActorLocation());

		// handle the point/battery change
		float fPointVal = dynamic_cast<ACleanableActor*>(OtherActor)->GetPointValue();
		m_fTotalProgress += fPointVal;
		m_fCurProgress += fPointVal;
		if (m_fCurProgress > m_fMaxProgress)
		{
			m_fCurProgress = 0.0f;
			m_fCurBattery += (m_fMaxProgress * 1.5f);
		}
		else
		{
			m_fCurBattery += (fPointVal / 2.0f);
		}
		if (m_fCurBattery > m_fMaxBattery)
			m_fCurBattery = m_fMaxBattery;
	}
}

void ARoombaActor::OnGameStart()
{
	// listen for various events
	m_pRoombaMesh->OnComponentBeginOverlap.AddDynamic(this, &ARoombaActor::OnCollisionOverlap);

	// play the vaccuum sound
	m_pAudioPlayer->Sound = m_pRoombaSound;
	m_pAudioPlayer->Play();

	// restart our progress
	m_fCurBattery = m_fMaxBattery;
	m_fCurProgress = 0.0f;
	m_fTotalProgress = 0.0f;
}

void ARoombaActor::OnGameOver()
{
	// no more collision hits
	m_pRoombaMesh->OnComponentBeginOverlap.RemoveDynamic(this, &ARoombaActor::OnCollisionOverlap);

	// play the game over sound
	m_pAudioPlayer->Stop();
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), m_pRoombaDeadSound, GetActorLocation());

	// notify we are done
	// TODO: Have objects listening to thing just listen to the game over event
	OnRoombaOutOfBattery.Broadcast(this);
}

// Called when the game starts or when spawned
void ARoombaActor::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ARoombaActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ARoombaActor::ChangePitch(float fPitch)
{
	m_pRoombaSound->PitchMultiplier = fPitch;
}

float ARoombaActor::DrainBattery(float DeltaTime)
{
	m_fCurBattery -= m_fBatteryDrainRate * DeltaTime;
	return m_fCurBattery;
}

float ARoombaActor::UpdateAliveTime(float DeltaTime)
{
	m_fTotalTime += DeltaTime;
	return m_fTotalTime;
}