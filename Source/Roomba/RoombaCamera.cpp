// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaCamera.h"


ARoombaCamera::ARoombaCamera() : Super()
{
	PrimaryActorTick.bCanEverTick = true;
	m_fMinDistance = 1000.0f;
	m_fDamping = 1.0f;
	m_fFOVWeight = 0.5f;
}


void ARoombaCamera::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator::MakeFromEuler(FVector(0.0f, 270.0f, 0.0f)));
}

void ARoombaCamera::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (m_ActorsToFollow.Num() > 0)
	{
		// get the average XY pos for all watched objects
		FVector v3AveragePos = FVector::ZeroVector;
		for (AActor* pActor : m_ActorsToFollow)
		{
			v3AveragePos += pActor->GetActorLocation();
		}
		v3AveragePos /= m_ActorsToFollow.Num();

		// get the bounds around the average point
		float fBiggestDist = 0.0f;
		for (AActor* pActor : m_ActorsToFollow)
		{
			FVector v3Pos = pActor->GetActorLocation();
			float fDist = FVector::DistSquared(v3Pos, v3AveragePos);
			if (fDist > fBiggestDist)
			{
				fBiggestDist = fDist;
			}
		}
		
		// determine the Z dist to show all watched objects
		float fAngle = 90.0f - ((GetCameraComponent()->FieldOfView * m_fFOVWeight) / GetCameraComponent()->AspectRatio);
		fAngle = FMath::DegreesToRadians(fAngle);
		v3AveragePos.Z = FMath::Max(m_fMinDistance, (FMath::Sqrt(fBiggestDist) * FMath::Tan(fAngle)));


		// apply the damping from our current pos to this new pos
		FVector v3FinalPos = GetActorLocation() + ((v3AveragePos - GetActorLocation()) * m_fDamping);

		// set our final pos
		SetActorLocation(v3FinalPos);

	}

}

void ARoombaCamera::FollowActor(AActor* pActor)
{
	m_ActorsToFollow.Add(pActor);
}

void ARoombaCamera::UnfollowActor(AActor* pActor)
{
	m_ActorsToFollow.Remove(pActor);
}
