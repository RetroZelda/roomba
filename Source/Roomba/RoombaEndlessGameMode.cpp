// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "Spawner.h"
#include "RoombaActor.h"
#include "WaitToStartHUD.h"
#include "RoombaController.h"
#include "RoombaEndlessGameMode.h"

ARoombaEndlessGameMode::ARoombaEndlessGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = NULL;// ARoombaActor::StaticClass();
	PlayerControllerClass = ARoombaController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> hudStart(TEXT("WidgetBlueprint'/Game/HUD/TouchToStartHUD.TouchToStartHUD_C'"));
	m_pStartHudClass = hudStart.Object;

	// TODO: Grab from the game instance, main menu, or generally anywhere that isnt here
	m_szLevelToLoad = "SingleLayout0";
}

void ARoombaEndlessGameMode::LoadInPlayers()
{
	Super::LoadInPlayers();

	// spawn a new roomba
	AActor* pStart = ChoosePlayerStart(nullptr);
	m_pRoomba = GetWorld()->SpawnActor<ARoombaActor>(pStart->GetActorLocation(), pStart->GetActorRotation());
}

void ARoombaEndlessGameMode::LoadInHud()
{
	Super::LoadInHud();
	m_pStartHUD = CreateWidget<UWaitToStartHUD>(GetWorld(), m_pStartHudClass);
}


void ARoombaEndlessGameMode::OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState)
{
	Super::OnScreenTouchEvent(fTouchX, fTouchY, bTouchState);
	if (bTouchState == false) // touch release
	{
		SetNewState(EGameRunningState::RUNNING);
	}
}

void ARoombaEndlessGameMode::OnLoadingStart()
{
	Super::OnLoadingStart();
}

void ARoombaEndlessGameMode::OnLoadingComplete()
{
	Super::OnLoadingComplete();
}

void ARoombaEndlessGameMode::HandleStateEnter(EGameRunningState eState)
{
	switch (eState)
	{
	case EGameRunningState::WAITING_TO_START:
	{
		// have our roomba register for my events
		OnGameStart.AddDynamic(m_pRoomba, &ARoombaActor::OnGameStart);
		OnGameOver.AddDynamic(m_pRoomba, &ARoombaActor::OnGameOver);

		// bind to the controller's tap event
		ARoombaController* pController = dynamic_cast<ARoombaController*>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		pController->OnScreenTouch.AddDynamic(this, &ARoombaEndlessGameMode::OnScreenTouchEvent);

		// show the new HUD
		m_pStartHUD->AddToViewport();
		m_pStartHUD->StartAnimationSequence();
		break;
	}
	case EGameRunningState::RUNNING:
	{
		ARoombaController* pController = dynamic_cast<ARoombaController*>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		pController->Possess(m_pRoomba);

		// start all our spawners
		for (TActorIterator<ASpawner> spawnerIter(GetWorld()); spawnerIter; ++spawnerIter)
		{
			spawnerIter->StartSpawner();
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		// have our roomba unregister for my events
		OnGameStart.RemoveDynamic(m_pRoomba, &ARoombaActor::OnGameStart);
		OnGameOver.RemoveDynamic(m_pRoomba, &ARoombaActor::OnGameOver);
		break;
	}
	}
	Super::HandleStateEnter(eState);
}

void ARoombaEndlessGameMode::HandleStateExit(EGameRunningState eState)
{
	switch (eState)
	{
	case EGameRunningState::FADE_IN:
	{
		break;
	}
	case EGameRunningState::WAITING_TO_START:
	{
		// unbind from the controller's tap event
		ARoombaController* pController = dynamic_cast<ARoombaController*>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		pController->OnScreenTouch.RemoveDynamic(this, &ARoombaEndlessGameMode::OnScreenTouchEvent);

		m_pStartHUD->RemoveFromViewport();

		break;
	}
	case EGameRunningState::RUNNING:
	{
		// stop all our spawners
		for (TActorIterator<ASpawner> spawnerIter(GetWorld()); spawnerIter; ++spawnerIter)
		{
			spawnerIter->StopSpawner();
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		break;
	}
	}
	Super::HandleStateExit(eState);
}

void ARoombaEndlessGameMode::HandleStateTick(EGameRunningState eState, float DeltaSeconds)
{
	switch (eState)
	{
	case EGameRunningState::FADE_IN:
	{
		break;
	}
	case EGameRunningState::WAITING_TO_START:
	{
		break;
	}
	case EGameRunningState::RUNNING:
	{
		m_pRoomba->UpdateAliveTime(DeltaSeconds);
		if (m_pRoomba->DrainBattery(DeltaSeconds) < 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("GAME OVER!"));
			SetNewState(EGameRunningState::GAMEOVER);
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		break;
	}
	}
	Super::HandleStateTick(eState, DeltaSeconds);
}


