// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "MazeObject.generated.h"

UENUM(BlueprintType)
enum class EMazeTileType : uint8
{									//// NOTES:
	Wall_Ghost_Door = 0x00,			//// - Represents 1st nibble (bits: 3 2 1 0)
	Wall_Horizontal = 0x01,			//// bits:
	Wall_Verticle = 0x02,			//// 0 - if floor then on means pellet
	Wall_N_E = 0x03,				//// 1 - if floor and 0 bit then on means power pellet
	Wall_N_W = 0x04,				//// 2 - if floor then on means ghost only(ghost house)
	Wall_S_E = 0x05,				//// 3 - On if FLOOR | Off if WALL
	Wall_S_W = 0x06,				//// 
	Invalid = 0x07,                 //// if wall then bits [2 1 0] are wall type
	Floor_Empty = 0x08,             //// 
	Floor_Pellet = 0x09,            //// 
	Floor_Power_Pellet = 0x0B,      //// 
	Floor_Ghost_Only = 0x0C,        ////            
};									//// 


UCLASS(Blueprintable)
class ROOMBA_API UMazeObject : public UObject
{
	GENERATED_BODY()

	UMazeObject();
	~UMazeObject();
	
	UPROPERTY(VisibleAnywhere, Category = "Maze Asset")
	uint32 _nMazeWidth;

	UPROPERTY(VisibleAnywhere, Category = "Maze Asset")
	uint32 _nMazeHeight;

	UPROPERTY(VisibleAnywhere, Category = "Maze Asset")
	uint32 _nPlayerStartIndex[5];

	UPROPERTY(VisibleAnywhere, Category = "Maze Asset")
	TArray<EMazeTileType> _TileMap;

	UPROPERTY(VisibleAnywhere, AdvancedDisplay, Category = "Maze Asset")
	TArray<uint8> _pRaw;

	void SplitTile(uint8& tileInfoIn, EMazeTileType& leftOut, EMazeTileType& rightOut);

public:
	bool Initialize(const void* const InMazeData, const int32 InMazeDataSizeBytes);
	
	uint32 GetWidth() { return _nMazeWidth; }
	uint32 GetHeight() { return _nMazeHeight; }
	uint32 GetTileCount() { return _TileMap.Num(); }
	uint32 GetPlayerStart(int32 nIndex) { return _nPlayerStartIndex[nIndex]; }

	EMazeTileType GetTile(int32 nIndex) { return _TileMap[nIndex]; }

};













