// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Factories/Factory.h"
#include "MazeObjectFactory.generated.h"

/**
 * 
 */
UCLASS()
class ROOMBA_API UMazeObjectFactory : public UFactory
{
	GENERATED_BODY()
		UMazeObjectFactory();

	// Begin UFactory Interface
	virtual UObject* FactoryCreateBinary(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, const TCHAR* Type, const uint8*& Buffer, const uint8* BufferEnd, FFeedbackContext* Warn) override;
	// End UFactory Interface
	
};
