// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Maze.generated.h"

UCLASS()
class ROOMBA_API AMaze : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class AMazeTile> _pFloorTile;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class AMazeTile> _pWallTile;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class AMazeTile> _pCornerTile;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class AMazeTile> _pDoorTile;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class ACleanableActor> _pPellet;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class ACleanableActor> _pPowerPellet;

	UPROPERTY(EditAnywhere, Category = "Maze")
	TSubclassOf<class ARoombaPlayerStart> _pPlayerStart;
	
	UPROPERTY(EditAnywhere, Category = "Maze")
	class UMazeObject* _pCurrentMazeLayout;

	UPROPERTY(EditAnywhere, Category = "Maze", meta = (ClampMin = "10.0", ClampMax = "1000.0", UIMin = "10.0", UIMax = "1000.0"))
	float _fGridSize;

private:
	int32 _nPelletCount;
	int32 _nPowerPelletCount;
	
public:	
	// Sets default values for this actor's properties
	AMaze();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void BuildMaze(UMazeObject* pMaze);
	
};
