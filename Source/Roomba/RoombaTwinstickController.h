// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "RoombaController.h"
#include "RoombaTwinstickController.generated.h"

class URoombaHUDBase;
class AArrowActor;
/**
 * 
 */
UCLASS()
class ROOMBA_API ARoombaTwinstickController : public APlayerController
{
	// TODO: This is based on the current RoombaController(that will be the base; acts like the future RoombaTouchController)
	//		 and will need updated when the controller hierarchy gets updated.
	GENERATED_UCLASS_BODY()

public:
	enum EMovementState { IDLE, TURN, MOVE, DEAD };

	UPROPERTY(BlueprintAssignable, Category = "Roomba Controller")
	FScreenTouchEvent OnScreenTouch;

private:

	AArrowActor* m_pArrow;

	EMovementState m_eState;
	UClass* m_pArrowBP;
	float m_fTurnRate;

	UClass *m_pGameOverHUDClass;

	bool m_bTouchState;

	FVector m_v3MoveAxis;
	FVector m_v3AimAxis;
	float m_fPrevMoveAngle;
	float m_fMoveAngle;
	float m_fAimAngle;

	void HandleMoveForward(float fVal);
	void HandleMoveRight(float fVal);
	void HandleAimForward(float fVal);
	void HandleAimRight(float fVal);
	void HandleActionButtonDown();
	void HandleActionButtonUp();

	UFUNCTION()
		void OnRoombaOutOfBattery(ARoombaActor* pRoomba);

protected:

	UFUNCTION(Category = "Roomba Controller")
		virtual void AddToCamera(APawn* pAttachPawn);

	UFUNCTION(Category = "Roomba Controller")
		virtual void RemoveFromCamera(APawn* pAttachPawn);

public:

	virtual void Possess(APawn* aPawn) override;
	virtual void UnPossess() override;
	virtual void BeginPlay() override;

	void SetupInputComponent();
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE EMovementState GetMovementState() { return m_eState; }

};
