// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Camera/CameraActor.h"
#include "RoombaCamera.generated.h"

/**
 * 
 */
UCLASS(Category = "Roomba")
class ROOMBA_API ARoombaCamera : public ACameraActor
{
	GENERATED_BODY()
	
	ARoombaCamera();

	UPROPERTY(EditAnywhere, Category = "Roomba Camera")
	float m_fMinDistance;

	UPROPERTY(EditAnywhere, Category = "Roomba Camera", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float m_fDamping;

	UPROPERTY(EditAnywhere, Category = "Roomba Camera", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float m_fFOVWeight;

	UPROPERTY(EditAnywhere, Category = "Roomba Camera")
	TArray<AActor*> m_ActorsToFollow;

public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION(BlueprintCallable, Category = "Roomba Camera")
	void FollowActor(AActor* pActor);

	UFUNCTION(BlueprintCallable, Category = "Roomba Camera")
	void UnfollowActor(AActor* pActor);
	
};
