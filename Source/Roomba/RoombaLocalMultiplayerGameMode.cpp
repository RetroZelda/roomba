// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "Spawner.h"
#include "RoombaActor.h"
#include "WaitToStartHUD.h"
#include "RoombaTwinstickController.h"
#include "RoombaLocalMultiplayerGameMode.h"
#include "RoombaPlayerStart.h"



ARoombaLocalMultiplayerGameMode::ARoombaLocalMultiplayerGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = NULL;// ARoombaActor::StaticClass();
	PlayerControllerClass = ARoombaTwinstickController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> hudStart(TEXT("WidgetBlueprint'/Game/HUD/TouchToStartHUD.TouchToStartHUD_C'"));
	m_pStartHudClass = hudStart.Object;

	// TODO: Grab from the game instance, main menu, or generally anywhere that isnt here
	m_szLevelToLoad = "MultiplayerLayout0";
}

void ARoombaLocalMultiplayerGameMode::LoadInPlayers()
{
	Super::LoadInPlayers();

	// spawn 4 players
	uint32 SpawnedPlayers = 0;
	for (TActorIterator<ARoombaPlayerStart> startIter(GetWorld()); startIter; ++startIter)
	{
		if ((SpawnedPlayers & (1 << startIter->PlayerIndex)) == 0)
		{
			APlayerController* pController = UGameplayStatics::GetPlayerController(this, startIter->PlayerIndex);
			if (pController == nullptr)
			{
				pController = UGameplayStatics::CreatePlayer(this, startIter->PlayerIndex);
			}
			ARoombaActor* pRoomba = GetWorld()->SpawnActor<ARoombaActor>(startIter->GetActorLocation(), startIter->GetActorRotation());
			m_Players.Add(PawnControllerLink(pController, pRoomba));
			SpawnedPlayers |= (1 << startIter->PlayerIndex);			
		}
	}

	for (ULevelStreaming* level : GetWorld()->StreamingLevels)
	{
		level->GetName();
	}
}

void ARoombaLocalMultiplayerGameMode::LoadInHud()
{
	Super::LoadInHud();
	m_pStartHUD = CreateWidget<UWaitToStartHUD>(GetWorld(), m_pStartHudClass);
}


void ARoombaLocalMultiplayerGameMode::OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState)
{
	Super::OnScreenTouchEvent(fTouchX, fTouchY, bTouchState);
	if (bTouchState == false) // touch release
	{
		SetNewState(EGameRunningState::RUNNING);
	}
}

void ARoombaLocalMultiplayerGameMode::OnLoadingStart()
{
	Super::OnLoadingStart();
}

void ARoombaLocalMultiplayerGameMode::OnLoadingComplete()
{
	Super::OnLoadingComplete();
}

void ARoombaLocalMultiplayerGameMode::HandleStateEnter(EGameRunningState eState)
{
	switch (eState)
	{
	case EGameRunningState::WAITING_TO_START:
	{
		// have our roomba register for my events
		for (PawnControllerLink link : m_Players)
		{
			OnGameStart.AddDynamic(link.pRoomba, &ARoombaActor::OnGameStart);
			OnGameOver.AddDynamic(link.pRoomba, &ARoombaActor::OnGameOver);
		}

		// bind to the controller's tap event
		ARoombaTwinstickController* pController = dynamic_cast<ARoombaTwinstickController*>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		pController->OnScreenTouch.AddDynamic(this, &ARoombaLocalMultiplayerGameMode::OnScreenTouchEvent);

		// show the new HUD
		m_pStartHUD->AddToViewport();
		m_pStartHUD->StartAnimationSequence();
		break;
	}
	case EGameRunningState::RUNNING:
	{
		for (PawnControllerLink link : m_Players)
		{
			link.pController->Possess(link.pRoomba);
		}

		// start all our spawners
		for (TActorIterator<ASpawner> spawnerIter(GetWorld()); spawnerIter; ++spawnerIter)
		{
			spawnerIter->StartSpawner();
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		// have our roomba unregister for my events
		for (PawnControllerLink link : m_Players)
		{
			OnGameStart.RemoveDynamic(link.pRoomba, &ARoombaActor::OnGameStart);
			OnGameOver.RemoveDynamic(link.pRoomba, &ARoombaActor::OnGameOver);
		}
		break;
	}
	}
	Super::HandleStateEnter(eState);
}

void ARoombaLocalMultiplayerGameMode::HandleStateExit(EGameRunningState eState)
{
	switch (eState)
	{
	case EGameRunningState::FADE_IN:
	{
		break;
	}
	case EGameRunningState::WAITING_TO_START:
	{
		// unbind from the controller's tap event
		ARoombaTwinstickController* pController = dynamic_cast<ARoombaTwinstickController*>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		pController->OnScreenTouch.RemoveDynamic(this, &ARoombaLocalMultiplayerGameMode::OnScreenTouchEvent);

		m_pStartHUD->RemoveFromViewport();

		break;
	}
	case EGameRunningState::RUNNING:
	{
		// stop all our spawners
		for (TActorIterator<ASpawner> spawnerIter(GetWorld()); spawnerIter; ++spawnerIter)
		{
			spawnerIter->StopSpawner();
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		break;
	}
	}
	Super::HandleStateExit(eState);
}

void ARoombaLocalMultiplayerGameMode::HandleStateTick(EGameRunningState eState, float DeltaSeconds)
{
	switch (eState)
	{
	case EGameRunningState::FADE_IN:
	{
		break;
	}
	case EGameRunningState::WAITING_TO_START:
	{
		break;
	}
	case EGameRunningState::RUNNING:
	{
		for (PawnControllerLink link : m_Players)
		{
			link.pRoomba->UpdateAliveTime(DeltaSeconds);

			// our exit condition.
			// TODO: Check when there is only 1 roomba left alive
			if (link.pRoomba->DrainBattery(0.0f/*DeltaSeconds*/) < 0.0f && eState == EGameRunningState::RUNNING)
			{
				UE_LOG(LogTemp, Warning, TEXT("GAME OVER!"));
				SetNewState(EGameRunningState::GAMEOVER);
			}
		}
		break;
	}
	case EGameRunningState::GAMEOVER:
	{
		break;
	}
	case EGameRunningState::FADE_OUT:
	{
		break;
	}
	}
	Super::HandleStateTick(eState, DeltaSeconds);
}




