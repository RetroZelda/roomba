// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerStart.h"
#include "RoombaPlayerStart.generated.h"

/**
 * 
 */
UCLASS()
class ROOMBA_API ARoombaPlayerStart : public APlayerStart
{
	GENERATED_BODY()

public:

		// The index of the player that will be starting here
		UPROPERTY(EditAnywhere, Category = "Roomba Player Start")
		int32 PlayerIndex;
	
	
};
