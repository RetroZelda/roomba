// Fill out your copyright notice in the Description page of Project Settings.

#include "Roomba.h"
#include "RoombaTwinstickController.h"
#include "RoombaActor.h"
#include "RoombaHUDBase.h"
#include "ArrowActor.h"
#include "RoombaCamera.h"



ARoombaTwinstickController::ARoombaTwinstickController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bAutoManageActiveCameraTarget = false;
	bShowMouseCursor = true;
	m_bTouchState = false;

	// get the game over hud
	static ConstructorHelpers::FObjectFinder<UClass> hudGameOver(TEXT("WidgetBlueprint'/Game/HUD/GameOverHUD.GameOverHUD_C'"));
	m_pGameOverHUDClass = hudGameOver.Object;

	// get hte arrow BP
	static ConstructorHelpers::FObjectFinder<UClass> arrowBP(TEXT("Blueprint'/Game/Arrow_Blueprint.Arrow_Blueprint_C'"));
	m_pArrowBP = (UClass*)arrowBP.Object;
	m_pArrow = nullptr;

	m_fTurnRate = 270.0f;
	m_eState = EMovementState::IDLE;
}

void ARoombaTwinstickController::SetupInputComponent()
{
	UE_LOG(LogTemp, Warning, TEXT("ARoombaTwinstickController::SetupInputComponent()"));

	// set up gameplay key bindings
	Super::SetupInputComponent();

	m_v3MoveAxis = m_v3AimAxis = FVector::ZeroVector;

	// bind our actions
	InputComponent->BindAction("ActionButton", EInputEvent::IE_Pressed, this, &ARoombaTwinstickController::HandleActionButtonDown);
	InputComponent->BindAction("ActionButton", EInputEvent::IE_Released, this, &ARoombaTwinstickController::HandleActionButtonUp);
	
	// bind out axis
	InputComponent->BindAxis("MoveForward", this, &ARoombaTwinstickController::HandleMoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARoombaTwinstickController::HandleMoveRight);
	InputComponent->BindAxis("AimForward", this, &ARoombaTwinstickController::HandleAimForward);
	InputComponent->BindAxis("AimRight", this, &ARoombaTwinstickController::HandleAimRight);
	
	m_bTouchState = false;
}

void ARoombaTwinstickController::AddToCamera(APawn* pAttachPawn)
{
	// NOTE: If there are multiple roomba cameras, the last one in this iterator will be what we are set to.
	for (TActorIterator<ARoombaCamera> iter(GetWorld()); iter; ++iter)
	{
		iter->FollowActor(pAttachPawn);
		this->SetViewTarget(*iter);
	}
}

void ARoombaTwinstickController::RemoveFromCamera(APawn* pAttachPawn)
{
	for (TActorIterator<ARoombaCamera> iter(GetWorld()); iter; ++iter)
	{
		iter->UnfollowActor(pAttachPawn);
	}
	this->SetViewTarget(nullptr);
}

void ARoombaTwinstickController::Possess(APawn* aPawn)
{
	// set the arrow on the controlling pawn
	UWorld* const pWorld = GetWorld();
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = aPawn;
	m_pArrow = pWorld->SpawnActor<AArrowActor>(m_pArrowBP, SpawnParams);
	m_pArrow->SetActorLocation(aPawn->GetActorLocation());

	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(aPawn);
	pPawn->OnRoombaOutOfBattery.AddDynamic(this, &ARoombaTwinstickController::OnRoombaOutOfBattery);

	m_eState = EMovementState::IDLE;

	AddToCamera(aPawn);

	Super::Possess(aPawn);
}

void ARoombaTwinstickController::UnPossess()
{
	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn)
	{
		pPawn->OnRoombaOutOfBattery.RemoveDynamic(this, &ARoombaTwinstickController::OnRoombaOutOfBattery);
		RemoveFromCamera(pPawn);
	}

	if (m_pArrow)
	{
		m_pArrow->Destroy();
		m_pArrow = nullptr;
	}

	m_eState = EMovementState::DEAD;

	Super::UnPossess();
}

void ARoombaTwinstickController::BeginPlay()
{
	Super::BeginPlay();

	m_eState = EMovementState::DEAD;
}

void ARoombaTwinstickController::Tick(float DeltaTime)
{
	ARoombaActor* pPawn = dynamic_cast<ARoombaActor*>(GetPawn());
	if (pPawn && m_eState != DEAD)
	{

		m_fPrevMoveAngle = m_fMoveAngle;
		m_fMoveAngle = FMath::RadiansToDegrees(m_v3MoveAxis.HeadingAngle());
		m_fAimAngle = FMath::RadiansToDegrees(m_v3AimAxis.HeadingAngle());
		
		m_pArrow->SetActorLocation(pPawn->GetActorLocation() + FVector(0.0f, 0.0f, 25.0f));
		m_pArrow->SetActorRotation(FRotator::MakeFromEuler(FVector(0.0f, 0.0f, m_fAimAngle)));
		m_pArrow->SetActorHiddenInGame(m_v3AimAxis.Size() < 0.01f);

		switch (m_eState)
		{
		case EMovementState::IDLE:
		{
			if (m_v3MoveAxis.Size() > 0.01f)
			{
				m_eState = EMovementState::TURN;
				pPawn->ChangePitch(2.0f);
			}

			break;
		}
		case EMovementState::TURN:
		{
			if (m_v3MoveAxis.Size() < 0.01f)
			{
				m_eState = EMovementState::IDLE;
				pPawn->ChangePitch(0.5f);
				break;
			}

			FRotator rMyRot = pPawn->GetActorRotation();
			FRotator rDestRot(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, m_fMoveAngle)));
			FRotator rFinalRot = FMath::RInterpConstantTo(rMyRot, rDestRot, DeltaTime, m_fTurnRate);

			pPawn->SetActorRotation(rFinalRot);
			if (rFinalRot.Equals(rDestRot))
			{
				m_eState = EMovementState::MOVE;
				pPawn->ChangePitch(1.0f);
			}

			break;
		}
		case EMovementState::MOVE:
		{
			if (m_v3MoveAxis.Size() < 0.01f)
			{
				m_eState = EMovementState::IDLE;
				break;
			}
			else if (FMath::Abs(m_fMoveAngle - m_fPrevMoveAngle) > 45.0f)
			{
				m_eState = EMovementState::TURN;
				pPawn->ChangePitch(2.0f);
				break;
			}
			FRotator rMyRot = pPawn->GetActorRotation();
			FRotator rDestRot(FQuat::MakeFromEuler(FVector(0.0f, 0.0f, m_fMoveAngle)));
			FRotator rFinalRot = FMath::RInterpConstantTo(rMyRot, rDestRot, DeltaTime, m_fTurnRate);

			pPawn->SetActorRotation(rFinalRot);
			pPawn->AddMovementInput(pPawn->GetActorForwardVector(), 0.75f);
			break;
		}
		};
	}
}

void ARoombaTwinstickController::OnRoombaOutOfBattery(ARoombaActor* pRoomba)
{
	// m_pArrow->SetActorHiddenInGame(true);
	m_eState = EMovementState::DEAD;

	// show the game over hud
	URoombaHUDBase* pHUD = CreateWidget<URoombaHUDBase>(this, m_pGameOverHUDClass);
	pHUD->AddToViewport();
}

void ARoombaTwinstickController::HandleActionButtonDown()
{
	UE_LOG(LogTemp, Warning, TEXT("ARoombaTwinstickController::HandleActionButtonDown()"));
	OnScreenTouch.Broadcast(0.0f, 0.0f, true);
}

void ARoombaTwinstickController::HandleActionButtonUp()
{
	UE_LOG(LogTemp, Warning, TEXT("ARoombaTwinstickController::HandleActionButtonUp()"));
	OnScreenTouch.Broadcast(0.0f, 0.0f, false);
}


void ARoombaTwinstickController::HandleMoveForward(float fVal)
{
	m_v3MoveAxis.X = fVal;
}

void ARoombaTwinstickController::HandleMoveRight(float fVal)
{
	m_v3MoveAxis.Y = fVal;
}

void ARoombaTwinstickController::HandleAimForward(float fVal)
{
	m_v3AimAxis.X = -fVal;
}

void ARoombaTwinstickController::HandleAimRight(float fVal)
{
	m_v3AimAxis.Y = fVal;
}



