// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "RoombaGameMode.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameEvent);


class UScreenFaderHUD;
class ULoadingScreenWidget;
class ARoombaCamera;
/**
 * 
 */
UCLASS()
class ROOMBA_API ARoombaGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()
	
	enum EGameRunningState {LOADING, FADE_IN, WAITING_TO_START, RUNNING, GAMEOVER, FADE_OUT, NONE};

	EGameRunningState m_eGameState;
	
	UClass *m_pFaderHudClass;
	UClass *m_pLoadingHudClass;
	UScreenFaderHUD* m_pFaderHUD;
	ULoadingScreenWidget* m_pLoadingHUD;

	TMap<FString, FString> m_GameOptions;
	FName m_szLevelToLoad;
	
	ARoombaCamera* m_pCamera;

public:

	// Called when the game has started(tapped screen)
	FGameEvent OnGameStart;

	// called when the game is over(Out of battery)
	FGameEvent OnGameOver;
	
private:

	UFUNCTION()
	void HandleStreamCompleted();

protected:

	// Override this function to ensure the proper options are set for each game mode.
	// Will only be called once per game mode.
	virtual void AssertGameOptions(TMap<FString, FString>& options);
	
	UFUNCTION()
	virtual void OnScreenTouchEvent(float fTouchX, float fTouchY, bool bTouchState);

	UFUNCTION()
	virtual void OnScreenFinishedFadeIn();

	UFUNCTION()
	virtual void OnScreenFinishedFadeOut();

	UFUNCTION()
	virtual void OnLoadingStart();

	UFUNCTION()
	virtual void OnLoadingComplete();
	
	virtual void HandleStateEnter(EGameRunningState eState);
	virtual void HandleStateExit(EGameRunningState eState);
	virtual void HandleStateTick(EGameRunningState eState, float DeltaSeconds);

	virtual void SpawnCamera();
	virtual void LoadInPlayers();
	virtual void LoadInHud();

public:

	virtual void BeginPlay() override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void Tick(float DeltaSeconds) override;

	void SetNewState(EGameRunningState eNewState);	

	EGameRunningState	GetGameRunningState()	{ return m_eGameState; }
	ARoombaCamera*		GetCamera()				{ return m_pCamera; }
	FString				GetOption(FString& var) { return m_GameOptions[var]; }
};
