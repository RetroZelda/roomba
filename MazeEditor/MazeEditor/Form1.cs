﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MazeEditor
{
    public partial class Form1 : Form
    {
        public enum TileType : byte          //// NOTES:
        {                                    //// - Represents 1st nibble (bits: 3 2 1 0)
            Invalid = 0x07,                  //// bits:
            Floor_Empty = 0x08,              //// 0 - if floor then on means pellet
            Floor_Pellet = 0x09,             //// 1 - if floor and 0 bit then on means power pellet
            Floor_Power_Pellet = 0x0B,       //// 2 - if floor then on means ghost only(ghost house)
            Floor_Ghost_Only = 0x0C,         //// 3 - On if FLOOR | Off if WALL
            Wall_Ghost_Door = 0x00,          //// 
            Wall_Horizontal = 0x01,          //// if wall then bits [2 1 0] are wall type
            Wall_Verticle = 0x02,            //// 
            Wall_N_E = 0x03,                 //// 
            Wall_N_W = 0x04,                 //// 
            Wall_S_E = 0x05,                 //// 
            Wall_S_W = 0x06                  //// 
        }

        private Bitmap _BackBuffer;
        private Dictionary<TileType, Bitmap> _TilePieces = new Dictionary<TileType, Bitmap>();
        private TileType _CurrentSelectedTile;
        public TileType CurrentSelectedTile
        {
            get
            {
                return _CurrentSelectedTile;
            }
            private set
            {
                _CurrentSelectedTile = value;
            }
        }

        private TileType[,] _TileMap;
        private Point _MousePos;
        private Point _HoverTile;
        private bool _bDrawSelectedGridLocation = false;
        private bool _bDrawTileOnMap = false;
        private int _SetStartIndex = -1;

        private bool _bEdits = false;
        private string _szFileName = "";

        public string FileName
        {
            get { return _szFileName; }
            set
            {
                _szFileName = value;
                this.Text = "Maze Editor: " + _szFileName;
                if(Edits)
                {
                    this.Text = this.Text + "*";
                }
            }
        }

        public bool Edits
        {
            get { return _bEdits; }
            set
            {
                if(_bEdits != value)
                {
                    _bEdits = value;
                    if(_bEdits)
                    {
                        if (FileName != "")
                        {
                            this.Text = this.Text + "*";
                        }
                    }
                    else
                    {
                        FileName = FileName;
                    }
                }
            }
        }

        private static byte CombinePieces(TileType tile0, TileType tile1)
        {
            return (byte)(((int)tile0 << 4 )| (int)tile1);
        }

        private void BreakPieces(byte rawTile, out TileType tile0, out TileType tile1)
        {
            tile0 = (TileType)(((int)rawTile & 0xF0) >> 4);
            tile1 = (TileType)((int)rawTile & 0x0F);
        }

        private Bitmap LoadBitmapResource(String szResourceName)
        {
            try
            {
                Assembly _assembly = Assembly.GetExecutingAssembly();
                Stream _imageStream = _assembly.GetManifestResourceStream(szResourceName);

                return new Bitmap(_imageStream);
            }
            catch
            {
                MessageBox.Show("Unable to load bitmap resource: " + szResourceName);
                return null;
            }

        }

        public Form1()
        {
            InitializeComponent();

            this.Text = "Maze Editor";

            _TilePieces[TileType.Invalid] = global::MazeEditor.Properties.Resources.invalid;
            _TilePieces[TileType.Floor_Empty] = global::MazeEditor.Properties.Resources.floor;
            _TilePieces[TileType.Floor_Pellet] = global::MazeEditor.Properties.Resources.pellet;
            _TilePieces[TileType.Floor_Power_Pellet] = global::MazeEditor.Properties.Resources.power_pellet;
            _TilePieces[TileType.Floor_Ghost_Only] = global::MazeEditor.Properties.Resources.ghost_only;
            _TilePieces[TileType.Wall_Ghost_Door] = global::MazeEditor.Properties.Resources.ghost_door;
            _TilePieces[TileType.Wall_Horizontal] = global::MazeEditor.Properties.Resources.horizontal;
            _TilePieces[TileType.Wall_Verticle] = global::MazeEditor.Properties.Resources.verticle;
            _TilePieces[TileType.Wall_N_E] = global::MazeEditor.Properties.Resources.N_E;
            _TilePieces[TileType.Wall_N_W] = global::MazeEditor.Properties.Resources.N_W;
            _TilePieces[TileType.Wall_S_E] = global::MazeEditor.Properties.Resources.S_E;
            _TilePieces[TileType.Wall_S_W] = global::MazeEditor.Properties.Resources.S_W;

            CurrentSelectedTile = TileType.Invalid;
            _TileMap = new TileType[(int)width.Value, (int)height.Value];
            currentLabel.Text = "Current: " + width.Value + " X " + height.Value;

            floorType.Text = floorType.Items[0].ToString();
            wallType.Text = wallType.Items[0].ToString();

            newToolStripMenuItem_Click(null, null);
        }

        private void UpdatePreview()
        {
            // determine if floor or wall
            if(floorButton.Checked)
            {
                switch(floorType.Text)
                {
                    case "Normal":
                        CurrentSelectedTile = TileType.Floor_Empty;
                        break;
                    case "Pellet":
                        CurrentSelectedTile = TileType.Floor_Pellet;
                        break;
                    case "Power Pellet":
                        CurrentSelectedTile = TileType.Floor_Power_Pellet;
                        break;
                    case "Ghost Only":
                        CurrentSelectedTile = TileType.Floor_Ghost_Only;
                        break;
                    default:
                        CurrentSelectedTile = TileType.Invalid;
                        break;
                }
            }
            else if (wallButton.Checked)
            {
                switch (wallType.Text)
                {
                    case "Horizontal":
                        CurrentSelectedTile = TileType.Wall_Horizontal;
                        break;
                    case "Verticle":
                        CurrentSelectedTile = TileType.Wall_Verticle;
                        break;
                    case "N -> E":
                        CurrentSelectedTile = TileType.Wall_N_E;
                        break;
                    case "N -> W":
                        CurrentSelectedTile = TileType.Wall_N_W;
                        break;
                    case "S -> E":
                        CurrentSelectedTile = TileType.Wall_S_E;
                        break;
                    case "S -> W":
                        CurrentSelectedTile = TileType.Wall_S_W;
                        break;
                    case "Ghost Door":
                        CurrentSelectedTile = TileType.Wall_Ghost_Door;
                        break;
                    default:
                        CurrentSelectedTile = TileType.Invalid;
                        break;
                }
            }
            else
            {
                // invalid
                CurrentSelectedTile = TileType.Invalid;
            }


            previewBox.Image = _TilePieces[CurrentSelectedTile];
            previewBox.Invalidate();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileName = "";
            Edits = false;

            // set the default tile
            for (int i = 0; i < _TileMap.GetLength(0); i++)
            {
                for (int j = 0; j < _TileMap.GetLength(1); j++)
                {
                    _TileMap[i, j] = TileType.Invalid;
                }
            }


            playerStartIndex0.Maximum = _TileMap.Length;
            playerStartIndex1.Maximum = _TileMap.Length;
            playerStartIndex2.Maximum = _TileMap.Length;
            playerStartIndex3.Maximum = _TileMap.Length;
            playerStartIndex4.Maximum = _TileMap.Length;

            playerStartIndex0.Value = 0;
            playerStartIndex1.Value = 1;
            playerStartIndex2.Value = 2;
            playerStartIndex3.Value = 3;
            playerStartIndex4.Value = 4;

            MazeDisplayPanel_SizeChanged(this, null);
        }

        // File Layout:
        //      Width
        //      Height
        //      Pac-Man start index
        //      Ghost Start Index[4]
        //      Tiles[Width X Height]
        //

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Maze Files (.maz)|*.maz";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;

            // Call the ShowDialog method to show the dialog box.
            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            FileName = openFileDialog1.FileName;
            FileStream fStream = new FileStream(FileName, FileMode.Open);
            BinaryReader read = new BinaryReader(fStream);

            // reading destinations
            int width, height;
            byte[] tiles;

            // read the dimensions
            this.width.Value = width = read.ReadInt32();
            this.height.Value = height = read.ReadInt32();

            // prep the starts
            playerStartIndex0.Maximum = (int)(width * height);
            playerStartIndex1.Maximum = (int)(width * height);
            playerStartIndex2.Maximum = (int)(width * height);
            playerStartIndex3.Maximum = (int)(width * height);
            playerStartIndex4.Maximum = (int)(width * height);

            // read the start indices
            playerStartIndex0.Value = read.ReadInt32();
            playerStartIndex1.Value = read.ReadInt32();
            playerStartIndex2.Value = read.ReadInt32();
            playerStartIndex3.Value = read.ReadInt32();
            playerStartIndex4.Value = read.ReadInt32();

            // read in the tile array
            tiles = read.ReadBytes((int)(((float)(width * height) / 2.0f) + 0.5f));

            // insert the tiles into the map
            _TileMap = new TileType[width, height];
            //TileType[] flatArray = new TileType[_TileMap.Length];
            int nTileIndex = 0;


            foreach(byte tile in tiles)
            {
                TileType left;
                TileType right;
                BreakPieces(tile, out left, out right);

                // insert into hte map
                if (nTileIndex < _TileMap.Length)
                {
                    _TileMap[nTileIndex / height, nTileIndex % height] = left;
                }
                nTileIndex++;
                if (nTileIndex < _TileMap.Length)
                {
                    _TileMap[nTileIndex / height, nTileIndex % height] = right;
                }
                nTileIndex++;
            }

            // we are done
            read.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(FileName == "")
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Maze File (.maz)|*.maz";
                saveFileDialog1.Title = "Save your Maze";
                if(saveFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    return;
                FileName = saveFileDialog1.FileName;
            }
            FileStream fStream = new FileStream(FileName, FileMode.OpenOrCreate);
            BinaryWriter write = new BinaryWriter(fStream);

            // write the dimentions
            write.Write((int)this.width.Value);
            write.Write((int)this.height.Value);

            // write hte start indices
            write.Write((int)playerStartIndex0.Value);
            write.Write((int)playerStartIndex1.Value);
            write.Write((int)playerStartIndex2.Value);
            write.Write((int)playerStartIndex3.Value);
            write.Write((int)playerStartIndex4.Value);

            // create the Tile array that will fit 
            // half of the WxH + 0.5 to round on truncate
            int width = _TileMap.GetLength(0);
            int height = _TileMap.GetLength(1);
            byte[] tiles = new byte[(int)(((float)(width * height) / 2.0f) + 0.5f)];

            // flatten into a 1D array
            TileType[] flatArray = new TileType[_TileMap.Length];
            for (int W = 0; W < width; ++W)
            {
                for (int H = 0; H < height; ++H)
                {
                    flatArray[W * height + H] = _TileMap[W, H];
                }
            }

            // compress into bytes
            int nTileCounter = 0;
            for (int nFlatIndex = 0; nFlatIndex < flatArray.Length; )
            {
                TileType left, right;
                left = flatArray[nFlatIndex];

                if(++nFlatIndex < flatArray.Length)
                {
                    right = flatArray[nFlatIndex++];
                }
                else
                {
                    right = TileType.Floor_Ghost_Only;
                }
                tiles[nTileCounter++] = CombinePieces(left, right);
            }

            // write the tiles
            write.Write(tiles);
            

            // we are done
            write.Flush();
            write.Close();
            Edits = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.DialogResult result = MessageBox.Show("You have unsaved changes.  Do you want to save?", "Warning", MessageBoxButtons.YesNoCancel);
            switch(result)
            {
                case System.Windows.Forms.DialogResult.Yes:
                    saveToolStripMenuItem_Click(null, null);
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                    return;
            }
            Application.Exit();
        }

        private void width_ValueChanged(object sender, EventArgs e)
        {

        }

        private void height_ValueChanged(object sender, EventArgs e)
        {

        }

        private void size_ValueChanged(object sender, EventArgs e)
        {
        }

        private void floorButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void wallButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void eraseButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void floorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void wallType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePreview();
        }

        private void MazeDisplayPanel_MouseMove(object sender, MouseEventArgs e)
        {
            _MousePos = e.Location;
            float fDim = (float)size.Value;
            _HoverTile.X = Math.Max((int)(Math.Min(_TileMap.GetLength(0) - 1, (int)(_MousePos.X / fDim)) * fDim), 0);
            _HoverTile.Y = Math.Max((int)(Math.Min(_TileMap.GetLength(1) - 1, (int)(_MousePos.Y / fDim)) * fDim), 0);

            if (_SetStartIndex > -1)
            {
                int X = (int)(_HoverTile.X / fDim);
                int Y = (int)(_HoverTile.Y / fDim);
                SetStartIndexValue(_SetStartIndex, (int)(X * height.Value + Y));
            }
            else if(_bDrawTileOnMap)
            {
                AddTileToMap();
            }
        }

        private void MazeDisplayPanel_MouseEnter(object sender, EventArgs e)
        {
            _bDrawSelectedGridLocation = true;
        }

        private void MazeDisplayPanel_MouseLeave(object sender, EventArgs e)
        {
            _bDrawSelectedGridLocation = false;
        }

        private void AddTileToMap()
        {
            float fDim = (float)size.Value;
            _TileMap[(int)(_HoverTile.X / fDim), (int)(_HoverTile.Y / fDim)] = CurrentSelectedTile;
            Edits = true;
        }

        private void MazeDisplayPanel_Paint(object sender, PaintEventArgs e)
        {

            int W = _TileMap.GetLength(0);
            int H = _TileMap.GetLength(1);
            Pen gridPen = new Pen(Color.White, 0.5f);
            float fDim = (float)size.Value;

            Graphics g = Graphics.FromImage(_BackBuffer);
            g.Clear(MazeDisplayPanel.BackColor);
            e.Graphics.Clear(MazeDisplayPanel.BackColor);

            for (int i = 0; i < W; i++)
            {
                for (int j = 0; j < H; j++)
                {
                    g.DrawImage(_TilePieces[_TileMap[i, j]], i * fDim, j * fDim, fDim, fDim);
                    if(showGridCheck.Checked)
                    {
                        g.DrawRectangle(gridPen, i * fDim, j * fDim, fDim, fDim);
                    }
                }
            }

            if(_bDrawSelectedGridLocation)
            {
                Pen mousePen = new Pen(Color.Red, 1.0f);
                g.DrawRectangle(mousePen, _HoverTile.X, _HoverTile.Y, fDim, fDim);
            }


            // draw our player starts
            Pen startPen = new Pen(Color.Yellow, 5.0f);
            g.DrawRectangle(startPen, (((int)playerStartIndex0.Value / H) * fDim), (((int)playerStartIndex0.Value % H) * fDim), fDim, fDim);
            startPen.Color = Color.Green;
            g.DrawRectangle(startPen, (((int)playerStartIndex1.Value / H) * fDim), (((int)playerStartIndex1.Value % H) * fDim), fDim, fDim);
            startPen.Color = Color.Blue;
            g.DrawRectangle(startPen, (((int)playerStartIndex2.Value / H) * fDim), (((int)playerStartIndex2.Value % H) * fDim), fDim, fDim);
            startPen.Color = Color.OrangeRed;
            g.DrawRectangle(startPen, (((int)playerStartIndex3.Value / H) * fDim), (((int)playerStartIndex3.Value % H) * fDim), fDim, fDim);
            startPen.Color = Color.Plum;
            g.DrawRectangle(startPen, (((int)playerStartIndex4.Value / H) * fDim), (((int)playerStartIndex4.Value % H) * fDim), fDim, fDim);

            // draw our back buffer
            e.Graphics.DrawImage(_BackBuffer, 0, 0);
            g.Dispose();

        }

        private void showGridCheck_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void MazeDisplayPanel_SizeChanged(object sender, EventArgs e)
        {
            _BackBuffer = new Bitmap(MazeDisplayPanel.Size.Width, MazeDisplayPanel.Size.Height);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MazeDisplayPanel.Invalidate();
        }

        private void MazeDisplayPanel_MouseDown(object sender, MouseEventArgs e)
        {
            _bDrawTileOnMap = true;
            if (_SetStartIndex < 0)
                AddTileToMap();
        }

        private void MazeDisplayPanel_MouseUp(object sender, MouseEventArgs e)
        {
            _bDrawTileOnMap = false;
            _SetStartIndex = -1;
        }

        private void ApplySizeButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("This will erase your current Maze.  Continue?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                _TileMap = new TileType[(int)width.Value, (int)height.Value];
                currentLabel.Text = "Current: " + width.Value + " X " + height.Value;

                string tempName = FileName;
                newToolStripMenuItem_Click(null, null);
                FileName = tempName;
                Edits = true;
            }
        }

        private void SetStartIndexValue(int nStartIndex, int nTileIndex)
        {
            switch(nStartIndex)
            {
                case 0:
                    playerStartIndex0.Value = nTileIndex;
                    break;
                case 1:
                    playerStartIndex1.Value = nTileIndex;
                    break;
                case 2:
                    playerStartIndex2.Value = nTileIndex;
                    break;
                case 3:
                    playerStartIndex3.Value = nTileIndex;
                    break;
                case 4:
                    playerStartIndex4.Value = nTileIndex;
                    break;
            }
        }

        private void start0button_Click(object sender, EventArgs e)
        {
            _SetStartIndex = 0;
        }

        private void start1button_Click(object sender, EventArgs e)
        {
            _SetStartIndex = 1;
        }

        private void start2button_Click(object sender, EventArgs e)
        {
            _SetStartIndex = 2;
        }

        private void start3button_Click(object sender, EventArgs e)
        {
            _SetStartIndex = 3;
        }

        private void start4button_Click(object sender, EventArgs e)
        {
            _SetStartIndex = 4;
        }
    }
}
