﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeEditor
{
    public static class Extentions
    {
        public static bool IsValid(this Form1.TileType e)
        {
            switch (e)
            {
                case Form1.TileType.Invalid:
                case Form1.TileType.Floor_Empty:
                case Form1.TileType.Floor_Pellet:
                case Form1.TileType.Floor_Power_Pellet:
                case Form1.TileType.Floor_Ghost_Only:
                case Form1.TileType.Wall_Ghost_Door:
                case Form1.TileType.Wall_Horizontal:
                case Form1.TileType.Wall_Verticle:
                case Form1.TileType.Wall_N_E:
                case Form1.TileType.Wall_N_W:
                case Form1.TileType.Wall_S_E:
                case Form1.TileType.Wall_S_W:
                    return true;
                default:
                    return false;
            }
        }
    }

}
