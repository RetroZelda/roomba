﻿namespace MazeEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.showGridCheck = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.size = new System.Windows.Forms.NumericUpDown();
            this.TIle = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.eraseButton = new System.Windows.Forms.RadioButton();
            this.floorButton = new System.Windows.Forms.RadioButton();
            this.wallButton = new System.Windows.Forms.RadioButton();
            this.PreviewGroupBox = new System.Windows.Forms.GroupBox();
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.wallBox = new System.Windows.Forms.GroupBox();
            this.wallType = new System.Windows.Forms.ComboBox();
            this.floorBox = new System.Windows.Forms.GroupBox();
            this.floorType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Sizebox = new System.Windows.Forms.GroupBox();
            this.currentLabel = new System.Windows.Forms.Label();
            this.ApplySizeButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.NumericUpDown();
            this.width = new System.Windows.Forms.NumericUpDown();
            this.MazeDisplayPanel = new MazeEditor.BufferedPanel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.playerStartIndex0 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.start0button = new System.Windows.Forms.Button();
            this.start1button = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.playerStartIndex1 = new System.Windows.Forms.NumericUpDown();
            this.start2button = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.playerStartIndex2 = new System.Windows.Forms.NumericUpDown();
            this.start3button = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.playerStartIndex3 = new System.Windows.Forms.NumericUpDown();
            this.start4button = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.playerStartIndex4 = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.size)).BeginInit();
            this.TIle.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.PreviewGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            this.wallBox.SuspendLayout();
            this.floorBox.SuspendLayout();
            this.Sizebox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex4)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1031, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.TIle);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.Sizebox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DimGray;
            this.splitContainer1.Panel2.Controls.Add(this.MazeDisplayPanel);
            this.splitContainer1.Size = new System.Drawing.Size(1031, 740);
            this.splitContainer1.SplitterDistance = 208;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.showGridCheck);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.size);
            this.groupBox2.Location = new System.Drawing.Point(12, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 68);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "View";
            // 
            // showGridCheck
            // 
            this.showGridCheck.AutoSize = true;
            this.showGridCheck.Location = new System.Drawing.Point(98, 42);
            this.showGridCheck.Name = "showGridCheck";
            this.showGridCheck.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.showGridCheck.Size = new System.Drawing.Size(75, 17);
            this.showGridCheck.TabIndex = 6;
            this.showGridCheck.Text = "Show Grid";
            this.showGridCheck.UseVisualStyleBackColor = true;
            this.showGridCheck.CheckedChanged += new System.EventHandler(this.showGridCheck_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Square";
            // 
            // size
            // 
            this.size.Location = new System.Drawing.Point(58, 16);
            this.size.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.size.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.size.Name = "size";
            this.size.Size = new System.Drawing.Size(120, 20);
            this.size.TabIndex = 4;
            this.size.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.size.ValueChanged += new System.EventHandler(this.size_ValueChanged);
            // 
            // TIle
            // 
            this.TIle.Controls.Add(this.groupBox1);
            this.TIle.Controls.Add(this.PreviewGroupBox);
            this.TIle.Controls.Add(this.wallBox);
            this.TIle.Controls.Add(this.floorBox);
            this.TIle.Location = new System.Drawing.Point(12, 406);
            this.TIle.Name = "TIle";
            this.TIle.Size = new System.Drawing.Size(188, 322);
            this.TIle.TabIndex = 2;
            this.TIle.TabStop = false;
            this.TIle.Text = "Tile";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.eraseButton);
            this.groupBox1.Controls.Add(this.floorButton);
            this.groupBox1.Controls.Add(this.wallButton);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(166, 48);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Type";
            // 
            // eraseButton
            // 
            this.eraseButton.AutoSize = true;
            this.eraseButton.Location = new System.Drawing.Point(108, 19);
            this.eraseButton.Name = "eraseButton";
            this.eraseButton.Size = new System.Drawing.Size(52, 17);
            this.eraseButton.TabIndex = 11;
            this.eraseButton.Text = "Erase";
            this.eraseButton.UseVisualStyleBackColor = true;
            this.eraseButton.CheckedChanged += new System.EventHandler(this.eraseButton_CheckedChanged);
            // 
            // floorButton
            // 
            this.floorButton.AutoSize = true;
            this.floorButton.Checked = true;
            this.floorButton.Location = new System.Drawing.Point(6, 19);
            this.floorButton.Name = "floorButton";
            this.floorButton.Size = new System.Drawing.Size(48, 17);
            this.floorButton.TabIndex = 9;
            this.floorButton.TabStop = true;
            this.floorButton.Text = "Floor";
            this.floorButton.UseVisualStyleBackColor = true;
            this.floorButton.CheckedChanged += new System.EventHandler(this.floorButton_CheckedChanged);
            // 
            // wallButton
            // 
            this.wallButton.AutoSize = true;
            this.wallButton.Location = new System.Drawing.Point(56, 19);
            this.wallButton.Name = "wallButton";
            this.wallButton.Size = new System.Drawing.Size(46, 17);
            this.wallButton.TabIndex = 10;
            this.wallButton.Text = "Wall";
            this.wallButton.UseVisualStyleBackColor = true;
            this.wallButton.CheckedChanged += new System.EventHandler(this.wallButton_CheckedChanged);
            // 
            // PreviewGroupBox
            // 
            this.PreviewGroupBox.Controls.Add(this.previewBox);
            this.PreviewGroupBox.Location = new System.Drawing.Point(6, 189);
            this.PreviewGroupBox.Name = "PreviewGroupBox";
            this.PreviewGroupBox.Size = new System.Drawing.Size(176, 128);
            this.PreviewGroupBox.TabIndex = 8;
            this.PreviewGroupBox.TabStop = false;
            this.PreviewGroupBox.Text = "Preview";
            // 
            // previewBox
            // 
            this.previewBox.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.previewBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewBox.Image = global::MazeEditor.Properties.Resources.invalid;
            this.previewBox.Location = new System.Drawing.Point(3, 16);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(170, 109);
            this.previewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewBox.TabIndex = 0;
            this.previewBox.TabStop = false;
            // 
            // wallBox
            // 
            this.wallBox.Controls.Add(this.wallType);
            this.wallBox.Location = new System.Drawing.Point(6, 131);
            this.wallBox.Name = "wallBox";
            this.wallBox.Size = new System.Drawing.Size(166, 52);
            this.wallBox.TabIndex = 7;
            this.wallBox.TabStop = false;
            this.wallBox.Text = "Wall";
            // 
            // wallType
            // 
            this.wallType.FormattingEnabled = true;
            this.wallType.Items.AddRange(new object[] {
            "Horizontal",
            "Verticle",
            "N -> E",
            "N -> W",
            "S -> E",
            "S -> W",
            "Ghost Door"});
            this.wallType.Location = new System.Drawing.Point(6, 19);
            this.wallType.Name = "wallType";
            this.wallType.Size = new System.Drawing.Size(154, 21);
            this.wallType.TabIndex = 10;
            this.wallType.SelectedIndexChanged += new System.EventHandler(this.wallType_SelectedIndexChanged);
            // 
            // floorBox
            // 
            this.floorBox.Controls.Add(this.floorType);
            this.floorBox.Location = new System.Drawing.Point(6, 73);
            this.floorBox.Name = "floorBox";
            this.floorBox.Size = new System.Drawing.Size(166, 52);
            this.floorBox.TabIndex = 6;
            this.floorBox.TabStop = false;
            this.floorBox.Text = "Floor";
            // 
            // floorType
            // 
            this.floorType.FormattingEnabled = true;
            this.floorType.Items.AddRange(new object[] {
            "Normal",
            "Pellet",
            "Power Pellet",
            "Ghost Only"});
            this.floorType.Location = new System.Drawing.Point(6, 19);
            this.floorType.Name = "floorType";
            this.floorType.Size = new System.Drawing.Size(154, 21);
            this.floorType.TabIndex = 9;
            this.floorType.SelectedIndexChanged += new System.EventHandler(this.floorType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(58, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Options";
            // 
            // Sizebox
            // 
            this.Sizebox.Controls.Add(this.currentLabel);
            this.Sizebox.Controls.Add(this.ApplySizeButton);
            this.Sizebox.Controls.Add(this.label3);
            this.Sizebox.Controls.Add(this.label2);
            this.Sizebox.Controls.Add(this.height);
            this.Sizebox.Controls.Add(this.width);
            this.Sizebox.Location = new System.Drawing.Point(12, 107);
            this.Sizebox.Name = "Sizebox";
            this.Sizebox.Size = new System.Drawing.Size(188, 124);
            this.Sizebox.TabIndex = 0;
            this.Sizebox.TabStop = false;
            this.Sizebox.Text = "Size";
            // 
            // currentLabel
            // 
            this.currentLabel.AutoSize = true;
            this.currentLabel.Location = new System.Drawing.Point(29, 16);
            this.currentLabel.Name = "currentLabel";
            this.currentLabel.Size = new System.Drawing.Size(44, 13);
            this.currentLabel.TabIndex = 7;
            this.currentLabel.Text = "Curent: ";
            // 
            // ApplySizeButton
            // 
            this.ApplySizeButton.Location = new System.Drawing.Point(57, 97);
            this.ApplySizeButton.Name = "ApplySizeButton";
            this.ApplySizeButton.Size = new System.Drawing.Size(75, 23);
            this.ApplySizeButton.TabIndex = 6;
            this.ApplySizeButton.Text = "Apply";
            this.ApplySizeButton.UseVisualStyleBackColor = true;
            this.ApplySizeButton.Click += new System.EventHandler(this.ApplySizeButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Height";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Width";
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(57, 71);
            this.height.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(120, 20);
            this.height.TabIndex = 1;
            this.height.Value = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.height.ValueChanged += new System.EventHandler(this.height_ValueChanged);
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(57, 45);
            this.width.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(120, 20);
            this.width.TabIndex = 0;
            this.width.Value = new decimal(new int[] {
            28,
            0,
            0,
            0});
            this.width.ValueChanged += new System.EventHandler(this.width_ValueChanged);
            // 
            // MazeDisplayPanel
            // 
            this.MazeDisplayPanel.BackColor = System.Drawing.Color.Gray;
            this.MazeDisplayPanel.Location = new System.Drawing.Point(30, 30);
            this.MazeDisplayPanel.Margin = new System.Windows.Forms.Padding(30);
            this.MazeDisplayPanel.Name = "MazeDisplayPanel";
            this.MazeDisplayPanel.Size = new System.Drawing.Size(759, 642);
            this.MazeDisplayPanel.TabIndex = 0;
            this.MazeDisplayPanel.SizeChanged += new System.EventHandler(this.MazeDisplayPanel_SizeChanged);
            this.MazeDisplayPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MazeDisplayPanel_Paint);
            this.MazeDisplayPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MazeDisplayPanel_MouseDown);
            this.MazeDisplayPanel.MouseEnter += new System.EventHandler(this.MazeDisplayPanel_MouseEnter);
            this.MazeDisplayPanel.MouseLeave += new System.EventHandler(this.MazeDisplayPanel_MouseLeave);
            this.MazeDisplayPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MazeDisplayPanel_MouseMove);
            this.MazeDisplayPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MazeDisplayPanel_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.start4button);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.playerStartIndex4);
            this.groupBox3.Controls.Add(this.start3button);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.playerStartIndex3);
            this.groupBox3.Controls.Add(this.start2button);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.playerStartIndex2);
            this.groupBox3.Controls.Add(this.start1button);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.playerStartIndex1);
            this.groupBox3.Controls.Add(this.start0button);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.playerStartIndex0);
            this.groupBox3.Location = new System.Drawing.Point(12, 237);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 163);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Player Starts";
            // 
            // playerStartIndex0
            // 
            this.playerStartIndex0.Location = new System.Drawing.Point(58, 28);
            this.playerStartIndex0.Name = "playerStartIndex0";
            this.playerStartIndex0.Size = new System.Drawing.Size(75, 20);
            this.playerStartIndex0.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Pac-Man";
            // 
            // start0button
            // 
            this.start0button.Location = new System.Drawing.Point(140, 25);
            this.start0button.Name = "start0button";
            this.start0button.Size = new System.Drawing.Size(38, 23);
            this.start0button.TabIndex = 10;
            this.start0button.Text = "Set";
            this.start0button.UseVisualStyleBackColor = true;
            this.start0button.Click += new System.EventHandler(this.start0button_Click);
            // 
            // start1button
            // 
            this.start1button.Location = new System.Drawing.Point(140, 50);
            this.start1button.Name = "start1button";
            this.start1button.Size = new System.Drawing.Size(38, 23);
            this.start1button.TabIndex = 13;
            this.start1button.Text = "Set";
            this.start1button.UseVisualStyleBackColor = true;
            this.start1button.Click += new System.EventHandler(this.start1button_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Player 1";
            // 
            // playerStartIndex1
            // 
            this.playerStartIndex1.Location = new System.Drawing.Point(58, 56);
            this.playerStartIndex1.Name = "playerStartIndex1";
            this.playerStartIndex1.Size = new System.Drawing.Size(75, 20);
            this.playerStartIndex1.TabIndex = 11;
            // 
            // start2button
            // 
            this.start2button.Location = new System.Drawing.Point(141, 79);
            this.start2button.Name = "start2button";
            this.start2button.Size = new System.Drawing.Size(38, 23);
            this.start2button.TabIndex = 16;
            this.start2button.Text = "Set";
            this.start2button.UseVisualStyleBackColor = true;
            this.start2button.Click += new System.EventHandler(this.start2button_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Player 2";
            // 
            // playerStartIndex2
            // 
            this.playerStartIndex2.Location = new System.Drawing.Point(58, 82);
            this.playerStartIndex2.Name = "playerStartIndex2";
            this.playerStartIndex2.Size = new System.Drawing.Size(75, 20);
            this.playerStartIndex2.TabIndex = 14;
            // 
            // start3button
            // 
            this.start3button.Location = new System.Drawing.Point(140, 108);
            this.start3button.Name = "start3button";
            this.start3button.Size = new System.Drawing.Size(38, 23);
            this.start3button.TabIndex = 19;
            this.start3button.Text = "Set";
            this.start3button.UseVisualStyleBackColor = true;
            this.start3button.Click += new System.EventHandler(this.start3button_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Player 3";
            // 
            // playerStartIndex3
            // 
            this.playerStartIndex3.Location = new System.Drawing.Point(58, 111);
            this.playerStartIndex3.Name = "playerStartIndex3";
            this.playerStartIndex3.Size = new System.Drawing.Size(75, 20);
            this.playerStartIndex3.TabIndex = 17;
            // 
            // start4button
            // 
            this.start4button.Location = new System.Drawing.Point(140, 134);
            this.start4button.Name = "start4button";
            this.start4button.Size = new System.Drawing.Size(38, 23);
            this.start4button.TabIndex = 22;
            this.start4button.Text = "Set";
            this.start4button.UseVisualStyleBackColor = true;
            this.start4button.Click += new System.EventHandler(this.start4button_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Player 4";
            // 
            // playerStartIndex4
            // 
            this.playerStartIndex4.Location = new System.Drawing.Point(59, 137);
            this.playerStartIndex4.Name = "playerStartIndex4";
            this.playerStartIndex4.Size = new System.Drawing.Size(75, 20);
            this.playerStartIndex4.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 764);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Maze Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.size)).EndInit();
            this.TIle.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.PreviewGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            this.wallBox.ResumeLayout(false);
            this.floorBox.ResumeLayout(false);
            this.Sizebox.ResumeLayout(false);
            this.Sizebox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerStartIndex4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox TIle;
        private System.Windows.Forms.RadioButton wallButton;
        private System.Windows.Forms.RadioButton floorButton;
        private System.Windows.Forms.GroupBox PreviewGroupBox;
        private System.Windows.Forms.GroupBox wallBox;
        private System.Windows.Forms.ComboBox wallType;
        private System.Windows.Forms.GroupBox floorBox;
        private System.Windows.Forms.ComboBox floorType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox Sizebox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown height;
        private System.Windows.Forms.NumericUpDown width;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.RadioButton eraseButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown size;
        private System.Windows.Forms.Label currentLabel;
        private System.Windows.Forms.Button ApplySizeButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox showGridCheck;
        private System.Windows.Forms.Timer timer1;
        private BufferedPanel MazeDisplayPanel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button start4button;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown playerStartIndex4;
        private System.Windows.Forms.Button start3button;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown playerStartIndex3;
        private System.Windows.Forms.Button start2button;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown playerStartIndex2;
        private System.Windows.Forms.Button start1button;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown playerStartIndex1;
        private System.Windows.Forms.Button start0button;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown playerStartIndex0;
    }
}

